<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessContact extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'business_contact';

    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }

    public function chat()
    {
        return $this->morphMany(ChatHistory::class, 'chatable');
    }

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    public function status()
    {
        return $this->belongsTo('App\ContactsStatus');
    }

    public function country()
    {
        return $this->belongsTo('App\Country');
    }

    public function project()
    {
        return $this->belongsTo('App\Project');
    }
}
