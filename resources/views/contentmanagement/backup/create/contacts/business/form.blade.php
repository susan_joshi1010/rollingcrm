<!-- text input -->
<div class="form-group">
    <label>Organization Name</label>
    {!! Form::text('organization_name', null, ['class' => 'form-control', 'placeholder' => 'Enter name']) !!}        
</div>
<div class="form-group">
    <label>Phone Number</label>
    {!! Form::number('phone_number', null, ['class' => 'form-control', 'placeholder' => 'Enter Phone Number']) !!}  
    <!-- <input type="number" class="form-control" placeholder="Enter Phone Number" name="phone_number"> -->
</div>
<div class="form-group">
    <label>Point of Contact</label>
    {!! Form::text('point_of_contact', null, ['class' => 'form-control', 'placeholder' => 'Enter Point of contact']) !!}        
</div>
<div class="form-group">
    <label>Point of Contact Number</label>
    {!! Form::number('point_of_contact_no', null, ['class' => 'form-control', 'placeholder' => 'Enter Point of Contact Number']) !!}  
    <!-- <input type="number" class="form-control" placeholder="Enter Phone Number" name="phone_number"> -->
</div>
<div class="form-group">
    <label>Email</label>
    {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Enter email']) !!}  
    <!-- <input type="email" class="form-control" placeholder="Enter Email" name="email"> -->
</div>
<div class="form-group">
    <label>Address</label>
    {!! Form::text('address', null, ['class' => 'form-control', 'placeholder' => 'Enter Address']) !!}  
    <!-- <input type="text" class="form-control" placeholder="Enter Address" name="address"> -->
</div>
<div class="form-group">
    <label>Country</label>
    {!! Form::select('country', $countries, null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    <label for="exampleInputFile">Documents</label>
    <input type="file" id="exampleInputFile" name="file">
    <!-- <p class="help-block">Example block-level help text here.</p> -->
</div>
<div class="form-group">
    <label>Status</label>
    {!! Form::select('status', $contact_status, null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    <label>Remarks</label>
    {!! Form::textarea('remarks', null, ['class' => 'form-control', 'rows' => '2',  "placeholder" => "Add Remarks"]) !!}
</div>