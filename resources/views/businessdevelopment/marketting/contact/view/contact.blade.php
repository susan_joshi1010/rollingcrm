@extends('layout.navbar')

@section('styles')
    <style>
    </style>
@endsection

@section('contents')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">View Project | {{$contact_type}} contact</h3>
                    <a href="{{route('marketting-show-project', $contact->project_id)}}" class="btn btn-primary btn-sm pull-right">
                        <i class="fa fa-arrow-left" aria-hidden="true"></i> Back 
                    </a>
                    @if($contact->accept_status == 0)
                        <div class="dropdown pull-right">
                            <button class="btn btn-default dropdown-toggle btn-sm" type="button" data-toggle="dropdown">Actions
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li><a href="#" disabled class="verify-contact" contact_type="{{$contact_type}}" contact_id="{{$contact->id}}" accept_status="1"><i class="fa fa-key" aria-hidden="true"></i> Activate</a></li>
                                <li><a href="#" disabled class="verify-contact" contact_type="{{$contact_type}}" contact_id="{{$contact->id}}" accept_status="2"><i class="fa fa-ban" aria-hidden="true"></i> Reject</a></li>
                            </ul>
                        </div>
                    @endif
                </div>
                <div class="box-body">
                    @if($contact_type == 'individual')
                        @include('businessdevelopment.marketting.contact.view.individual')
                    @elseif ($contact_type == 'business')
                        @include('businessdevelopment.marketting.contact.view.business')
                    @endif
                </div>
            </div>
        </div>   
    </div>
</section>
@endsection


@section('scripts')
    <script>
        $(document).ready( function () {
            $(".verify-contact").on("click", function(){
                Swal.fire({
                title: 'Are you sure?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes!'
                }).then((result) => {
                    if (result.value) {
                        axios.put('/business-developement/projects/contact/'+$(this).attr('contact_id')+'/'+$(this).attr('contact_type')+'/accept/'+$(this).attr('accept_status'))
                        .then(function (response) {
                            location.reload();
                        })
                        .catch(function (error) {
                            console.log(error);
                        });
                    }
                });
            });
        });
    </script>
@endsection

