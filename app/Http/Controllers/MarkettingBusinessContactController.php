<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Country;
use App\Project;
use App\IndividualContact;
use App\BusinessContact;
use App\ContactsStatus;
use DB;
use Yajra\Datatables\Datatables;
use App\MarkettingIndividualContact;
use App\MarkettingBusinessContact;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class MarkettingBusinessContactController extends Controller
{
    public function getBusinessContacts($project_id)
    {
        $business_contacts = MarkettingBusinessContact::where([
            ['project_id', $project_id]
        ])
        ->orderBy('created_at', 'DESC')
        ->with('status', 'country')
        ->get();
        $contact_status = ContactsStatus::pluck('title', 'id');
        return Datatables::of($business_contacts)
         ->addColumn('checkbox', function ($business_contacts)
         {
            $checkbox='<input type="checkbox" class="bus_select" contact-id='.$business_contacts->id.'><br>';
            return $checkbox;
         })
         ->addColumn('actions', function ($business_contacts)
         {
            $buttons = '<div class="dropdown">
                            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Actions
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li><a href="#" disabled contact_id='.$business_contacts->id.' class="bus-actions view-bus"><i class="fa fa-eye" aria-hidden="true"></i> View</a></li>
                                '.($business_contacts->accept_status == 0 ? '
                                    <li role="presentation" class="divider"></li>
                                    <li>
                                        <a href="#" disabled class="verify-bus-contact" contact_id='.$business_contacts->id.' contact_type="business" accept_status="1">
                                            <i class="fa fa-key" aria-hidden="true"></i> Activate
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" disabled class="verify-bus-contact" contact_id='.$business_contacts->id.' contact_type="business" accept_status="2">
                                            <i class="fa fa-ban" aria-hidden="true"></i> Reject
                                        </a>
                                    </li>
                                ': '').'
                            </ul>
                        </div>';
             return $buttons;
         })
         ->rawColumns(['checkbox', 'actions'])
         ->make(true);
    }

    public function createContact(Request $request, $slug)
    {
        $this->validate($request, [
            'organization_name' => 'required||max:255',
            'email' => 'required||email',
            'point_of_contact' => 'required',
            'point_of_contact_no' => 'required',
            'phone_number' => 'required',
            'status_id' => 'required',
            'address' => 'required',
        ]);

        DB::beginTransaction();
        try{
            $project = Project::where('slug', $slug)->first();

            $contact = MarkettingBusinessContact::create([
                'organization_name' => $request->organization_name,
                'phone_number' => $request->phone_number,
                'email' => $request->email,
                'point_of_contact' => $request->point_of_contact,
                'point_of_contact_no' => $request->point_of_contact_no,
                'address' => $request->address,
                'project_id' => $project->id,
                'country_id' => $request->country_id,
                'status_id' => $request->status_id,
                'remarks' => $request->remarks,
            ]);

            $file = $request->file('file');
            if ($file) {
                $extension = $file->getClientOriginalExtension();
                $filename = time().'.'.$extension;
                $original_filename = $file->getClientOriginalName();
                $url = 'documents/projects/'.$project->id.'/business/'.$filename;
                Storage::disk('public')->put($url,  File::get($file));
                $contact->image()
                    ->create(
                        [   
                            "url" => $url,
                            'original_filename' => $original_filename
                        ]
                    );
            }
            DB::commit();
            return redirect()->back()->with('success', 'Form Submitted Sucessfully.'); 

        }catch(\Exception $errors){
            DB::rollback();
            return back()->withErrors($errors->getMessage());
        }
    }

    public function showContact($contact_id)
    {
        $contact_type = 'business';
        $contact = MarkettingBusinessContact::where('id', $contact_id)->with('image')->first();
        $compact = ['contact_type', 'contact'];
        return view('businessdevelopment.marketting.contact.view.contact', compact($compact));
    }

    public function filterBusinessContact(Request $request)
    {
        $organization_name = $request->input('organization_name');
        $point_of_contact = $request->input('point_of_contact');
        $accept_status = $request->input('accept_status');
        $email = $request->input('email');
        $address = $request->input('address');
        $phone_number = $request->input('phone_number');
        $status_id = $request->input('status_id');
        $joined_from = $request->input('joined_from');
        $joined_till = $request->input('joined_till');
        $business_contacts = MarkettingBusinessContact::when($organization_name, function ($query) use ($organization_name) {
            return $query->where('organization_name', 'like', "%$organization_name%");
        })
        ->when($email, function ($query) use ($email) {
            return $query->where('email', $email);
        })
        // ->when($accept_status, function ($query) use ($accept_status) {
        //     return $query->where('accept_status', $accept_status);
        // })
        ->when($point_of_contact, function ($query) use ($point_of_contact) {
            return $query->where('point_of_contact', 'like', "%$point_of_contact%");
        })
        ->when($address, function ($query) use ($address) {
            return $query->where('address', "like", "%$address%");
        })
        ->when($phone_number, function ($query) use ($phone_number) {
            return $query->where('phone_number', $phone_number);
        })
        ->when($status_id, function ($query) use ($status_id) {
            if ($status_id != 'all-status') {
                return $query->where('status_id', $status_id);
            } else {
                return $query;
            }
        })
        ->when($joined_from, function ($query) use ($joined_from) {
            return $query->whereDate('created_at', '>=', $joined_from);
        })
        ->when($joined_till, function ($query) use ($joined_till) {
            return $query->whereDate('created_at', '<=', $joined_till);
        })
        ->where('project_id', $request->input('project_id'))
        ->orderBy('created_at', 'DESC')
        ->with('status', 'country')
        ->get();
            
        return Datatables::of($business_contacts)
        ->addColumn('checkbox', function ($business_contacts)
        {
           $checkbox='<input type="checkbox" class="bus_select" contact-id='.$business_contacts->id.'><br>';
           return $checkbox;
        })
        ->addColumn('actions', function ($business_contacts)
        {
            $buttons = '<div class="dropdown">
                            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Actions
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li><a href="#" disabled contact_id='.$business_contacts->id.' class="bus-actions view-bus"><i class="fa fa-eye" aria-hidden="true"></i> View</a></li>
                                '.($business_contacts->accept_status == 0 ? '
                                    <li role="presentation" class="divider"></li>
                                    <li>
                                        <a href="#" disabled class="verify-bus-contact" contact_id='.$business_contacts->id.' contact_type="business" accept_status="1">
                                            <i class="fa fa-key" aria-hidden="true"></i> Activate
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" disabled class="verify-bus-contact" contact_id='.$business_contacts->id.' contact_type="business" accept_status="2">
                                            <i class="fa fa-ban" aria-hidden="true"></i> Reject
                                        </a>
                                    </li>
                                ': '').'
                            </ul>
                        </div>';
                return $buttons;
        })
        ->rawColumns(['checkbox', 'actions'])
        ->make(true);
    }

    public function autoComplete(Request $request)
    {
        // dd($request->all(), $organization_name);
        $organization_name = $request->input('organization_name');
        $point_of_contact = $request->input('point_of_contact');
        $email = $request->input('email');
        $address = $request->input('address');
        $phone_number = $request->input('phone_number');
        
        $contacts = MarkettingBusinessContact::where('project_id', $request->project_id)
            ->when($organization_name, function ($query) use ($organization_name) {
                return $query->where('organization_name', 'like', "%$organization_name%")->pluck("organization_name");
            })
            ->when($email, function ($query) use ($email) {
                return $query->where('email', "like" , "%$email%")->pluck("email");
            })
            ->when($point_of_contact, function ($query) use ($point_of_contact) {
                return $query->where('point_of_contact', 'like', "%$point_of_contact%")->pluck("point_of_contact");
            })
            ->when($address, function ($query) use ($address) {
                return $query->where('address', "like", "%$address%")->pluck("address");
            })
            ->when($phone_number, function ($query) use ($phone_number) {
                return $query->where('phone_number', "like", "%$phone_number%")->pluck("phone_number");
            });
        return response()->json($contacts, 200);
    }
}
