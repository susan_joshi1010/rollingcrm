<?php

use Illuminate\Database\Seeder;

class MediumSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $medium = [
            ["medium" => "Internet", "order" => 1],
            ["medium" => "Telephone", "order" => 2],
            ["medium" => "Email", "order" => 3],
            ["medium" => "Direct Contact", "order" => 4],
        ];

        DB::table('medium')->insert($medium);
    }
}
