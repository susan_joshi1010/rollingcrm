@extends('mails.index')

@section('mail')
<div class="col-md-9">
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Compose New Message</h3>
        </div>
       
        <div class="box-body">
            <div class="form-group">
                 @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                         </ul>
                    </div>
                 @endif

                @if ($message = Session::get('success'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                             <strong>{{ $message }}</strong>
                    </div>
                @endif
                <form method="post" action="{{url('mail/send')}}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <!-- <label>Enter Your Name</label> -->
                            <input type="text" name="email" class="form-control" placeholder="To:" />
                    </div>

                    <div class="form-group">
                        <!-- <label>Enter Your Email</label> -->
                             <input type="text" name="subject" class="form-control" placeholder="Subject:" />
                    </div>

                    <div class="form-group">
                        <!-- <label>Enter Your Message</label> -->
                            <textarea id="compose" name="message" class="form-control" style="height: 300px"></textarea>
                    </div>
                    
                    <div class="box-footer">
                        <div class="pull-right">
                            <button type="submit" name="send" class="btn btn-primary"><i class="fa fa-envelope-o"></i> Send</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
         
</div>
@endsection

