@extends('layout.navbar')

@section('contents')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Manage Existing Business Contacts</h3>
                        <ul class="nav navbar-nav navbar-right">
                            <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Change Status <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                @foreach ($contact_status as $key => $contact_sts)
                                    <li><a href="#" class="change-status" status_id="{{ $key }}">{{ $contact_sts }}</a></li>
                                @endforeach
                            </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="box-body">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if (\Session::has('success'))
                            <div class="alert alert-success">
                                <ul>
                                    <li>{!! \Session::get('success') !!}</li>
                                </ul>
                            </div>
                        @endif
                        {!! Form::open(['route' => 'post-create-new-business-contact', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
                            @include('contentmanagement.project.manage.contacts.business.form')
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script>
        $(document).ready( function () {
        let Otable = $("#table");
        Otable.DataTable().destroy();  
        let table = Otable.DataTable({
            "processing": true,
            "serverSide": false,
            "pageLength": 50,
            ajax: '/api/content-management/project/manage-old/contacts/business/manage-old',
            columns: [
                { data: 'checkbox' },
                { data: 'created_at' },
                { data: 'organization_name' },
                { data: 'status.title' },
                { data: 'point_of_contact' },
                { data: 'point_of_contact_no' },
                { data: 'address' },
                { data: 'phone_number' },
                { data: 'email' },
                { data: 'country.name' },
                { data: 'actions' },
            ], 
            "columnDefs": [
            {
                "searchable": false,
                "targets": [0,6]
                }, 
                {
                "orderable": false,
                "targets": [0,6]
                },
            ],
            select:true,
            dom: 'Bfrtip',
            //   buttons: [
            //               {
            //                 extend: 'excel',
            //                 text: 'Export to Excel',
            //                 className: 'btn btn-default',
            //                 exportOptions: {
            //                     columns: 'th:not(:last-child)',
            //                     rows: { selected: true }
            //                 }
            //               },{
            //                 extend: 'pdf',
            //                 text: 'Export to PDF',
            //                 className: 'btn btn-default',
            //                 exportOptions: {
            //                     columns: 'th:not(:last-child)',
            //                     rows: { selected: true }
            //                 },
            //               },
            //             ]
            });
            table.on('draw.dt', function(){
               $(".update").on('click',function(){
                window.location.href = '/content-management/project/create-new/contacts/business/create-new/'+$(this).attr('contact-id');
               });
            
               $(".select_all").click(function() {
                  if($(this). prop("checked") == true){
                     $('.select').prop('checked', true);
                  }
                  if($(this). prop("checked") == false){
                     $('.select').prop('checked', false);
                  }
               });

            });
        } );

        $(".change-status").on('click', function(){
            let status_id = $(this).attr('status_id');
            var contacts = [];
            $.each($("input[class='select']:checked"), function(){
                contacts.push($(this).attr('contact-id'));
            });
            if (contacts.length>0) {
                axios.post('/api/contacts/business/update-status', {
                    status_id,
                    contacts
                })
                .then(function (response) {
                    console.log(response);
                    var table = $('#table').DataTable();
                    table.ajax.reload();
                })
                .catch(function (error) {
                    console.log(error);
                });
            } else {
                alert("Please choose the contacts first to update them!");
            }
        });
     </script>
@endsection