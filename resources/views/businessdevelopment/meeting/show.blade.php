@extends('layout.navbar')

@section('contents')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">View Meeting</h3>
                        <a href="{{url()->previous()}}" class="btn btn-primary btn-sm pull-right">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back 
                        </a>
                    </div>

                    <div class="box-body">
                        <div class="form-group col-md-4">
                        <h5><strong>Title:</strong></h5>
                        <div class="contents">{{$meeting->title}}</div>
                    </div>

                    <div class="form-group col-md-4">
                        <h5><strong>Meeting Time:</strong></h5>
                        <div class="contents">{{$meeting->time}}</div>
                    </div>

                    <div class="form-group col-md-4">
                        <h5><strong>Meeting Date:</strong></h5>
                        <div class="contents">{{$meeting->date}}</div>
                    </div>

                    <div class="form-group col-md-4">
                        <h5><strong>Venue:</strong></h5>
                        <div class="contents">{{$meeting->venue}}</div>
                    </div>

                    <div class="form-group col-md-12">
                        <h5><strong>Agenda:</strong></h5>
                        <div class="contents">{!! $meeting->agenda !!}</div>
                    </div>

                    <div class="form-group col-md-4">
                         <h5><strong>Estimated Meeting Period:</strong></h5>
                        <div class="contents">{{$meeting->estimated_meeting_period}}</div>
                     </div>
                                        
                </div>
            </div>   
        </div>
    </section>
  
@endsection