@extends('layout.navbar')

@section('contents')
<section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Meeting</h3>
                        <a href="{{url('/business-development/meeting/index')}}" class="btn btn-primary btn-sm pull-right">
                           <i class="fa fa-arrow-left" aria-hidden="true"></i> Back 
                        </a>
                    </div>
                    <div class="box-body">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if (\Session::has('success'))
                            <div class="alert alert-success">
                                <ul>
                                    <li>{!! \Session::get('success') !!}</li>
                                </ul>
                            </div>
                        @endif
                        
                            {!! Form::model($meeting, ['route' => ['meeting-update', $meeting->id], 'method' => 'PUT', 'enctype' => 'multipart/form-data']) !!}
                        
                            <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="title">Title:</label>
                                            {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Title']) !!}
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="time">Meeting Time:</label>
                                            {!! Form::time('time', null, ['class' => 'form-control', 'placeholder' => 'Time']) !!}
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="date">Meeting Date:</label>
                                            {!! Form::date('date', null, ['class' => 'form-control', 'placeholder' => 'date']) !!}
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="venue">Venue:</label>
                                            {!! Form::text('venue', null, ['class' => 'form-control', 'placeholder' => 'Venue']) !!}
                                    </div>
                                    <div class="form-group col-md-12">
                                         <label for="agenda">Agenda:</label>
                                            {!! Form::textarea('agenda', null, ['class' => 'form-control', 'rows' => '2']) !!}
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="estimated_meeting_period">Estimated Meeting Period:</label>
                                            {!! Form::time('estimated_meeting_period', null, ['class' => 'form-control', 'placeholder' => 'Estimated Meeting Period']) !!}
                                    </div>
                            </div>

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary pull-right">Submit</button>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>   
        </div>
    </section>
@endsection
@section('scripts')
<script>
    $(function () {
        CKEDITOR.replace( 'agenda' );
    })
</script>
@endsection