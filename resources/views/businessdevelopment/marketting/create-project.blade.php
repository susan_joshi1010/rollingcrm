@extends('layout.navbar')

@section('contents')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Create Project</h3>
                        <a href="{{route('marketting-projects')}}" class="btn btn-primary btn-sm pull-right">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back 
                        </a>
                    </div>
                    <div class="box-body">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if (\Session::has('success'))
                            <div class="alert alert-success">
                                <ul>
                                    <li>{!! \Session::get('success') !!}</li>
                                </ul>
                            </div>
                        @endif
                            
                        {!! Form::open(['route' => 'marketting-store-project', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
                        <div class="form-group col-md-6">
                            <label for="project_name">Project Name:</label>
                            {!! Form::text('project_name', null, ['class' => 'form-control', 'placeholder' => 'Project Name']) !!}
                        </div>
                        <div class="form-group col-md-6">
                            <label for="project_coordinator">Project Coordinator:</label>
                            {!! Form::text('project_coordinator', null, ['class' => 'form-control', 'placeholder' => 'Project Coordinator']) !!}
                        </div>
                        <div class="form-group col-md-12">
                            <label for="objective">Objective:</label>
                            {!! Form::textarea('objective', null, ['class' => 'form-control', 'rows' => '2']) !!}
                        </div>
                        <div class="form-group col-md-12">
                            <label for="document">Documents</label>
                            <input type="file" id="document" name="document[]" class="form-control-file" onchange="javascript:documentlist()" multiple>
                            <div id="documentlist"></div>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary pull-right">Submit</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>   
        </div>
    </section>
@endsection
@section('scripts')
<script>
    $(function () {
        CKEDITOR.replace( 'objective' );
    })

    documentlist = function() {
        var input = document.getElementById('document');
        var output = document.getElementById('documentlist');
        var selectedfiles = "";
        for (var i = 0; i < input.files.length; ++i) {
            selectedfiles += '<li>' + input.files.item(i).name + '</li>';
        }
        output.innerHTML = '<ul>'+selectedfiles+'</ul>';
    }
</script>
   
@endsection