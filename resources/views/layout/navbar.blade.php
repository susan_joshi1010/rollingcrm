@extends('layout.master')

@section('_styles')
    @yield('styles')
@endsection

@section('navbar')
    @if(Route::currentRouteName() !== 'marketting-create-contact')
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"> <span class="nav-label">Dashboard</span> <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li class="dropdown-submenu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <span class="nav-label">Metrices</span><span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="{{route('generate-report')}}">Generate Report</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"> <span class="nav-label">Contact Management</span> <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li class="dropdown-submenu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <span class="nav-label">Project</span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="{{route('manage-projects')}}">Manage Project</a></li>
                                        <li class="dropdown-submenu">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <span class="nav-label">Create Project</span></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="{{route('create-project', 'individual')}}">Individual Contract</a></li>
                                                <li><a href="{{route('create-project', 'business')}}">Business Contract</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"> <span class="nav-label">Message</span> <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li class="dropdown-submenu">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <span class="nav-label">Compose</span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="{{route('mail-compose')}}">Mail</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"> <span class="nav-label">Business Development</span> <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="{{route('marketting-projects')}}"><span class="nav-label">Marketing</span></a>
                                </li>
                                <li class="dropdown-submenu"> 
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <span class="nav-label">Meetings</span><span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="{{route('meeting-index')}}">Manage Meeting</a></li>
                                        <li><a href="{{route('create-meeting')}}">Create Meeting</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"> <span class="nav-label">Tickets</span> <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li class="dropdown-submenu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <span class="nav-label">Support Request</span><span class="caret"></span></a>
                                <!-- <ul class="dropdown-menu">
                                    <li><a href="#">Support Request</a></li>
                                </ul> -->
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
    @endif
    @yield('contents')
@endsection

@section('_scripts')
    @yield('scripts')
@endsection