<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactsStatus extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'contact_status';
}
