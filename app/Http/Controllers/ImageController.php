<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Image;
use App\Project;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class ImageController extends Controller
{
    public function deletefile($file_id) {
        $image = Image::where('id', $file_id)->delete();
        return response()->json(["message" => "file has been removed"], 200);
    }

    public function Updatefile(Request $request, $project_id) {
        // dd($request->all(), $project_id);
        $project = Project::find($project_id);
        // dd($project);
        $documents = $request->documents;
        if ($documents) {
            foreach ($documents as $key => $document) {
                $extension = $document->getClientOriginalExtension();
                $documentname = $key.time().'.'.$extension;
                $url = 'documents/projects/'.$project_id.'/'.$documentname;
                $original_filename = $document->getClientOriginalName();
                Storage::disk('public')->put($url,  File::get($document));
                $project->image()
                ->create(
                    [   
                        "url" => $url,
                        'original_filename' => $original_filename
                        ]
                    );
            }
        }
        return response()->json(["message" => "sucessfully updated", 'data' =>$project->image], 200);
    }
}
