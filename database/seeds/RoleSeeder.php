<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = [
            ["name" => "admin"],
            ["name" => "user"],
            ["name" => "super_admin"],
        ];

        DB::table('role')->insert($role);
    }
}
