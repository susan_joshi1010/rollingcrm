<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use App\Country;
// use App\Project;
// use App\Image;
use App\ChatHistory;
use App\IndividualContact;
use App\BusinessContact;
use App\User;
// use App\ContactsStatus;
// use Illuminate\Support\Facades\Storage;
// use Illuminate\Support\Facades\File;
use DB;
use Yajra\Datatables\Datatables;

class ChatHistoryController extends Controller
{
    public function storeChatHistory(Request $request, $contact_type, $contact_id) {
        $this->validate($request, [
            'contact_person' => 'required',
            'medium_id' => 'required',
            'interest_level_id' => 'required',
            'description' => 'required',
            'next_follow_up' => 'required',
        ]);

        if ($contact_type == "Individual") {
            IndividualContact::find($contact_id)
                ->chat()
                ->create([
                    "contact_person" => $request->contact_person,
                    "medium_id" => $request->medium_id,
                    "interest_level_id" => $request->interest_level_id,
                    "description" => $request->description,
                    "next_follow_up" => $request->next_follow_up,
                    "entered_by" => 1
                    ]);
                } else if ($contact_type == "Business") {
                    BusinessContact::find($contact_id)
                    ->chat()
                    ->create([
                        "contact_person" => $request->contact_person,
                        "medium_id" => $request->medium_id,
                        "interest_level_id" => $request->interest_level_id,
                        "description" => $request->description,
                        "next_follow_up" => $request->next_follow_up,
                        "entered_by" => 1
                ]);
        }
        return response()->json(["message" => "successfully chat history added."], 200);
    }

    public function getChatHistory($contact_type, $contact_id) {
        if ($contact_type == 'individual') {
            $chat = IndividualContact::find($contact_id)->chat()->with('medium', 'interestLevel', 'enteredBy');
        } else if ($contact_type == 'business') {
            $chat = BusinessContact::find($contact_id)->chat()->with('medium', 'interestLevel', 'enteredBy');
        }
        return Datatables::of($chat)
            ->addColumn('actions', function ($chat)
            {
                $buttons = '<div class="dropdown">
                            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Actions
                            <span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li role="presentation"><a href="" class="update-chat-history" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#chat-history" role="menuitem" tabindex="-1" chat-id="'.$chat->id.'"><i class="fa fa-refresh" aria-hidden="true"></i> Edit</a></li>
                                <li><a href="" class="delete-chat-history" chat-id="'.$chat->id.'"><i class="fa fa-trash" aria-hidden="true"></i> Delete</a></li>
                            </ul>
                        </div>';
                return $buttons;
            })
            ->rawColumns(['actions'])
            ->make(true);
    }

    public function showChatHistory($chat_id) {
        return response()->json(ChatHistory::where('id', $chat_id)->first(), 200);
    }

    public function updateChatHistory(Request $request, $chat_id) {
        $this->validate($request, [
            'contact_person' => 'required',
            'medium_id' => 'required',
            'interest_level_id' => 'required',
            'description' => 'required',
            'next_follow_up' => 'required',
        ]);

        $chat_history = ChatHistory::where('id', $chat_id)->update([
            "contact_person" => $request->contact_person,
            "medium_id" => $request->medium_id,
            "interest_level_id" => $request->interest_level_id,
            "description" => $request->description,
            "next_follow_up" => $request->next_follow_up
        ]);

        return response()->json(["message" => "sucessfully updated"],200);
    }

    public function deleteChatHistory($chat_id) {
        ChatHistory::where('id', $chat_id)->delete();
        return response()->json(["message" => "sucessfully removed"], 200);
    }
}
