@extends('layout.navbar')

@section('contents')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Business Contact Report</h3>
                        <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div id="myfirstchart" style="height: 250px;"></div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script>
        $(document).ready( function () {
            axios.get('/api/generate-report/contacts')
            .then(function (response) {
                // handle success
                console.log(response);
                Morris.Bar({
                    element: 'myfirstchart',
                    data: response.data.data,
                    xkey: 'y',
                    ykeys: ['a', 'b'],
                    labels: ['Individual', 'Business']
                });
            })
            .catch(function (error) {
                // handle error
                console.log(error);
            })
            .then(function () {
                // always executed
            });
        });
    </script>
@endsection