<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChatHistory extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'chat_history';

    public function chatable()
    {
        return $this->morphTo();
    }

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    public function medium()
    {
        return $this->belongsTo('App\Medium');
    }

    public function interestLevel()
    {
        return $this->belongsTo('App\InterestLevel');
    }

    public function enteredBy()
    {
        return $this->belongsTo('App\User', 'entered_by');
    }
}
