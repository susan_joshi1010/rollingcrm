<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            ["name" => "John Doe", "email" => "johndoe@rollings.com","password" => "password", "department_id" => 1, "role_id" => 1],
        ];

        DB::table('user')->insert($user);
    }
}
