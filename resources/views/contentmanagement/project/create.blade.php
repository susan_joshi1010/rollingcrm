@extends('layout.navbar')

@section('contents')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Create Project | {{$contact_type}} contact </h3>
                        <a href="{{url('/content-management/projects')}}" class="btn btn-primary btn-sm pull-right">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back 
                        </a>
                    </div>
                    <div class="box-body">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if (\Session::has('success'))
                            <div class="alert alert-success">
                                <ul>
                                    <li>{!! \Session::get('success') !!}</li>
                                </ul>
                            </div>
                        @endif
                        @if($contact_type == 'individual')
                            {!! Form::open(['route' => 'store-project-individual', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
                        @elseif ($contact_type == 'business')
                            {!! Form::open(['route' => 'store-project-business', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
                        @endif
                            @include('contentmanagement.project.form')
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary pull-right">Submit</button>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>   
        </div>
    </section>
@endsection
@section('scripts')
<script>
    $(function () {
        CKEDITOR.replace( 'objective' );
    })

    documentlist = function() {
        var input = document.getElementById('document');
        var output = document.getElementById('documentlist');
        var selectedfiles = "";
        for (var i = 0; i < input.files.length; ++i) {
            selectedfiles += '<li>' + input.files.item(i).name + '</li>';
        }
        output.innerHTML = '<ul>'+selectedfiles+'</ul>';
    }
</script>
   
@endsection