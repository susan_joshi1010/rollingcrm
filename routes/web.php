<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// generate report page
Route::get('/', 'DashboardController@home')->name('generate-report');
Route::get('/api/generate-report/contacts', 'DashboardController@generateContactsReport');
Route::get('/api/generate-report/contacts', 'DashboardController@generateContactsReport');
// generate report page

Route::get('/content-management/projects', 'ProjectController@index')->name('manage-projects');
Route::get('/api/content-management/projects', 'ProjectController@getProjects');

Route::get('/content-management/projects/create/{project_id}', 'ProjectController@show')->name('show-project');
Route::get('/api/content-management/projects/{project_id}/contacts/individual', 'IndividualContactsController@getIndividualContacts');
Route::get('/api/content-management/projects/{project_id}/contacts/business', 'BusinessContactController@getBusinessContacts');
Route::get('/content-management/projects/create/contact/{contact_type}', 'ProjectController@create')->name('create-project');
Route::post('/content-management/projects/create/create/contact/individual', 'IndividualContactsController@store')->name('store-project-individual');
Route::post('/content-management/projects/create/create/contact/business', 'BusinessContactController@store')->name('store-project-business');

Route::get('/content-management/projects/{project_id}/update/contact/{contact_id}/{contact_type}', 'ProjectController@showContact')->name('update-project');
Route::put('/content-management/projects/{project_id}/update/contact/{contact_id}/individual', 'IndividualContactsController@updateProject')->name('update-project-individual');
Route::put('/content-management/projects/{project_id}/update/contact/{contact_id}/business', 'BusinessContactController@updateProject')->name('update-project-business');

Route::get('/content-management/projects/{project_id}/create/contact/{contact_type}', 'ProjectController@addContactToExistingProject')->name('add-contact-to-existing-project');

Route::put('/content-management/projects/{project_id}/update', 'ProjectController@update')->name('update-project-details');

Route::post('/api/contacts/{contact_type}/update-status', 'ContactStatusController@updateStatus');


Route::post('/api/contacts/individual/filter', 'IndividualContactsController@filterIndividualContact');
Route::post('/api/contacts/business/filter', 'BusinessContactController@filterBusinessContact');

Route::post('/api/content-management/projects/individual/autocomplete', 'IndividualContactsController@autoComplete');
 
//Send Mail
Route::get('/mail/compose','MailController@compose')->name('mail-compose');
Route::post('/mail/send','MailController@send');
//Send Mail

Route::post('/api/content-management/projects/business/autocomplete', 'BusinessContactController@autoComplete');

Route::post('/content-management/projects/contact/{contact_type}/{contact_id}/chat-history', 'ChatHistoryController@storeChatHistory');


Route::get('/content-management/projects/{project_id}/contact/individual/{contact_id}', 'IndividualContactsController@showContact');

Route::get('/content-management/projects/{project_id}/contact/business/{contact_id}', 'BusinessContactController@showContact');


Route::get('/content-management/projects/contact/{contact_type}/{contact_id}/chat-history', 'ChatHistoryController@getChatHistory');

Route::get('/content-management/projects/contact/chat-history/{chat_id}', 'ChatHistoryController@showChatHistory');
Route::post('/api/content-management/projects/contact/chat-history/{chat_id}', 'ChatHistoryController@updateChatHistory');
Route::delete('/api/content-management/projects/contact/chat-history/{chat_id}', 'ChatHistoryController@deleteChatHistory');

Route::delete('/api/content-management/projects/file/{file_id}', 'ImageController@deletefile');
Route::post('/api/content-management/projects/{project_id}/file', 'ImageController@Updatefile');

//business development meeting
Route::get('/business-development/meeting/create', 'MeetingController@create')->name('create-meeting');
Route::post('/business-development/meeting/create', 'MeetingController@store')->name('store-meeting');
Route::get('/business-development/meeting/index', 'MeetingController@index')->name('meeting-index');
Route::put('/business-development/meeting/update/{id}', 'MeetingController@update')->name('meeting-update');
Route::get('/business-development/meeting/edit/{id}', 'MeetingController@edit')->name('meeting-edit');
// Route::delete('/business-development/meeting/delete/{id}', 'MeetingController@delete');
Route::get('/business-development/meeting/show/{id}', 'MeetingController@show')->name('meeting-show');





Route::get('/business-development/marketting/projects/create', 'MarkettingController@create')->name('marketting-create-project');
Route::post('/business-development/marketting/projects/store', 'MarkettingController@store')->name('marketting-store-project');
Route::get('/business-development/marketting/projects', 'MarkettingController@index')->name('marketting-projects');
Route::get('/api/business-development/marketting/projects', 'MarkettingController@projects');

Route::get('/business-development/marketting/project/{project_id}', 'MarkettingController@showProject')->name('marketting-show-project');
Route::get('/api/business-development/marketting/projects/{project_id}/contacts/individual', 'MarkettingIndividualContactController@getIndividualContacts');
Route::get('/api/business-development/marketting/projects/{project_id}/contacts/business', 'MarkettingBusinessContactController@getBusinessContacts');

Route::get('/business-developement/projects/{slug}/create/contact/{contact_type}', 'MarkettingController@createContact')->name('marketting-create-contact');
Route::post('/business-developement/projects/{slug}/create/contact/individual', 'MarkettingIndividualContactController@createContact')->name('marketting-store-individual');
Route::post('/business-developement/projects/{slug}/create/contact/business', 'MarkettingBusinessContactController@createContact')->name('marketting-store-business');

Route::get('/business-developement/projects/create/contact/{contact_id}/individual', 'MarkettingIndividualContactController@showContact');
Route::get('/business-developement/projects/create/contact/{contact_id}/business', 'MarkettingBusinessContactController@showContact');
Route::put('/business-developement/projects/contact/{contact_id}/{contact_type}/accept/{accept_status}', 'MarkettingController@acceptContact');
Route::post('/api/business-developement/projects/contact/{contact_type}/accept/{accept_status}', 'MarkettingController@acceptMultipleContact');
Route::post('/api/business-developement/marketting/project/contacts/individual/filter', 'MarkettingIndividualContactController@filterIndividualContact');
Route::post('/api/business-developement/marketting/project/contacts/individual/filter/autocomplete', 'MarkettingIndividualContactController@autoComplete');
Route::post('/api/business-developement/marketting/project/contacts/business/filter', 'MarkettingBusinessContactController@filterBusinessContact');
Route::post('/api/business-developement/marketting/project/contacts/business/filter/autocomplete', 'MarkettingBusinessContactController@autoComplete');

