<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use App\MarkettingIndividualContact;
use App\MarkettingBusinessContact;
use DB;
use Yajra\Datatables\Datatables;
use App\Country;
use App\Medium;
use App\InterestLevel;
use App\IndividualContact;
use App\BusinessContact;
use App\ContactsStatus;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class MarkettingController extends Controller
{
    public function index()
    {
        return view('businessdevelopment.marketting.project');
    }

    public function projects()
    {
        $projects = Project::whereNotNull('slug')->get();
        
        return Datatables::of($projects)
        ->addColumn('actions', function ($projects)
        {
            $buttons='<button type="button" project-id="'.$projects->id.'" class="view btn btn-primary" title="View"><i class="fa fa-eye" aria-hidden="true"></i> View</button>';
            return $buttons;
        })
        ->rawColumns(['actions'])
        ->make(true);;
    }

    public function showProject($project_id)
    {
        $countries = Country::pluck('name', 'id');
        $project = Project::where('id', $project_id)->with('image')->first();
        $contact_status = ContactsStatus::pluck('title', 'id');
        $medium = Medium::pluck('medium', 'id');
        $interest_level = InterestLevel::pluck('interest_level', 'id');
        $compact = ['countries', 'contact_status', 'project_id', 'project', 'medium', 'interest_level'];
        return view('businessdevelopment.marketting.contact', compact($compact));
    }
    
    public function create()
    {
        return view('businessdevelopment.marketting.create-project');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'project_name' => 'required||max:255',
            'objective' => 'required',
            'project_coordinator' => 'required||max:255'
        ]);

        DB::beginTransaction();
        try{
            $project = Project::create([
                'project_name' => $request->project_name,
                'project_coordinator' => $request->project_coordinator,
                'objective' => $request->objective,
                'slug' => str_random(4).time()
            ]);

            $documents = $request->file('document');
            if ($documents) {
                foreach ($documents as $key => $document) {
                    $extension = $document->getClientOriginalExtension();
                    $documentname = $key.time().'.'.$extension;
                    $url = 'documents/projects/'.$project->id.'/'.$documentname;
                    $original_filename = $document->getClientOriginalName();
                    Storage::disk('public')->put($url,  File::get($document));
                    $project->image()
                    ->create(
                        [   
                            "url" => $url,
                            'original_filename' => $original_filename
                            ]
                        );
                }
            }

            DB::commit();
            return redirect()->back()->with('success', 'Project created sucessfully.');

        }catch(\Exception $errors){
            DB::rollback();
            return back()->withErrors($errors->getMessage());
        }
    }

    public function createContact($slug, $contact_type)
    {
        $countries = Country::pluck('name', 'id');
        $contact_status = ContactsStatus::pluck('title', 'id');
        $project = Project::where('slug', $slug)->with('image')->first();
        $compact = ['countries', 'contact_status', 'contact_type', 'slug', 'project'];
        return view('businessdevelopment.marketting.create-contact', compact($compact));
    }

    public function acceptContact($contact_id, $contact_type, $accept_status)
    {
        if ($contact_type == 'individual') {
            $contact = MarkettingIndividualContact::where('id', $contact_id)->first();
            if ($accept_status == 1)
            {
                $this->createIndividualContact($contact);
            }
            $contact->update(['accept_status' => $accept_status]);
        } else if ($contact_type == 'business') {
            $contact = MarkettingBusinessContact::where('id', $contact_id)->first();
            if ($accept_status == 1)
            {
                $this->createBusinessContact($contact);
            }
            $contact->update(['accept_status' => $accept_status]);
        }
        return response()->json(['message' => 'sucessfully updated.'], 200);
    }

    public function acceptMultipleContact(Request $request, $contact_type, $accept_status)
    {
        if ($contact_type == 'individual') {
            foreach ($request->contact_id as $key => $id) {
                $contact = MarkettingIndividualContact::where('id', $id)->first();
                if ($accept_status == 1)
                {
                    $this->createIndividualContact($contact);
                }
                $contact->update(['accept_status' => $accept_status]);
            }
        } else if ($contact_type == 'business') {
            foreach ($request->contact_id as $key => $id) {
                $contact = MarkettingBusinessContact::where('id', $id)->first();
                if ($accept_status == 1)
                {
                    $this->createBusinessContact($contact);
                }
                $contact->update(['accept_status' => $accept_status]);
            }    
        }
        return response()->json(['message' => 'sucessfully updated.'], 200);
    }

    public function createIndividualContact($contact)
    {
        IndividualContact::create([
            "marketing_individual_contact_id" => $contact->id,
            "name" => $contact->name,
            "phone_number" => $contact->phone_number,
            "email" => $contact->email,
            "address" => $contact->address,
            "country_id" => $contact->country_id,
            "project_id" => $contact->project_id,
            "status_id" => $contact->status_id,
            "remarks" => $contact->remarks
        ]);
    }

    public function createBusinessContact($contact)
    {
        BusinessContact::create([
            "marketing_business_contact_id" => $contact->id,
            "organization_name" => $contact->organization_name,
            "point_of_contact" => $contact->point_of_contact,
            "point_of_contact_no" => $contact->point_of_contact_no,
            "phone_number" => $contact->phone_number,
            "email" => $contact->email,
            "address" => $contact->address,
            "country_id" => $contact->country_id,
            "project_id" => $contact->project_id,
            "status_id" => $contact->status_id,
            "remarks" => $contact->remarks
        ]);
    }
}
