@extends('layout.navbar')

@section('styles')
    <style>
    </style>
@endsection

@section('contents')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">View Project | {{$contact_type}} contact</h3>
                    <a href="{{url()->previous()}}" class="btn btn-primary btn-sm pull-right">
                        <i class="fa fa-arrow-left" aria-hidden="true"></i> Back 
                    </a>
                </div>
                <div class="box-body">
                    @if($contact_type == 'individual')
                        @include('contentmanagement.contact.view.individual')
                    @elseif ($contact_type == 'business')
                        @include('contentmanagement.contact.view.business')
                    @endif
                    <div class="form-group col-md-12">
                        <h3>Chat History</h3>
                    </div>
                    <table id="chat_history_table" class="display" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Contact Person</th>
                                <th>Medium</th>
                                <th>Interest Level</th>
                                <th width="18%">Description</th>
                                <th>Next Follow-up</th>
                                <th>Added At</th>
                                <th>Added By</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                    </table>    
                </div>
            </div>
        </div>   
    </div>
</section>
<div class="modal fade" id="chat-history" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-comments-o" aria-hidden="true"></i> Edit Chat History</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-md-12" id="validation-errors"></div>  
                    <div class="form-group col-md-4">
                        <label>Contact Person</label>
                        {!! Form::text('contact_person', null, ['class' => 'form-control chat-history', 'placeholder' => 'Contact Person']) !!}        
                    </div>
                    <div class="form-group col-md-4">
                        <label>Medium</label>
                        {!! Form::select('medium_id', $medium, null, ['class' => 'form-control chat-history']) !!}
                    </div>
                    <div class="form-group col-md-4">
                        <label>Interest Level</label>
                        {!! Form::select('interest_level_id', $interest_level, null, ['class' => 'form-control chat-history']) !!}
                    </div>
                    <div class="form-group col-md-12">
                        <label for="description">Description:</label>
                        {!! Form::textarea('description', null, ['class' => 'form-control chat-history', 'rows' => '2']) !!}
                    </div>
                    <div class="form-group col-md-4">
                        <label>Next Follow-up</label>
                        <input type="date" class="form-control chat-history" id="next_follow_up" name="next_follow_up" placeholder="Next Follow Up">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="update-chat-history"><i class="fa fa-refresh" aria-hidden="true"></i> Edit</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                Are You sure You want to remove this Chat Log?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <a class="btn btn-danger btn-ok">Delete</a>
            </div>
        </div>
    </div>
</div>
@endsection


@section('scripts')
    <script>
        $(document).ready( function () {
            
            $(function () {
                CKEDITOR.replace( 'description' );
            })
            let table = $("#chat_history_table");
            table.DataTable().destroy();
            let chat_history_table = table.DataTable({
                "processing": true,
                "serverSide": false,
                "pageLength": 50,
                ajax: {
                    "url" : '/content-management/projects/contact/{{$contact_type}}/{{$contact_id}}/chat-history',
                },
                order: [ [5, 'desc'] ], 
                columns: [
                    { data: 'contact_person' },
                    { data: 'medium.medium' },
                    { data: 'interest_level.interest_level' },
                    { data: 'description' },
                    { data: 'next_follow_up' },
                    { data: 'created_at' },
                    { data: 'entered_by.name' },
                    { data: 'actions' }
                ], 
                "columnDefs": [
                    {
                        "searchable": false,
                        "targets": [6]
                    }, 
                    {
                        "orderable": false,
                        "targets": [6]
                    },
                    {
                        "render": function ( data, type, row ) {
                            // var regex = /(&nbsp;|<([^>]+)>)/ig
                            // ,   body = data
                            // ,   result = body.replace(regex, "");
                            // return result;
                            return data; 
                        },
                        "targets": 3
                    },
                ],
                select:true,
                dom: 'Bfrtip',
            });
            chat_history_table.on('draw.dt', function(){
                $(".update-chat-history").on('click', function(){
                    let chat_id = $(this).attr('chat-id');
                    axios.get('/content-management/projects/contact/chat-history/'+chat_id)
                    .then(function (response) {
                        let data = response.data;
                        $("input[name=contact_person].chat-history").val(data.contact_person);
                        $("select[name=medium_id].chat-history").val(data.medium_id);
                        $("select[name=interest_level_id].chat-history").val(data.interest_level_id);
                        CKEDITOR.instances['description'].setData(data.description);
                        $("input[name=next_follow_up].chat-history").val(data.next_follow_up);
                        $('#update-chat-history').attr('chat-id', chat_id);
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
                });

                $(".delete-chat-history").on("click", function(e){
                    e.preventDefault();
                    let that = $(this);
                    const swalWithBootstrapButtons = Swal.mixin({
                    customClass: {
                        confirmButton: 'btn btn-success',
                        cancelButton: 'btn btn-danger'
                    },
                    buttonsStyling: false
                    })
                    swalWithBootstrapButtons.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    reverseButtons: true
                    }).then((result) => {
                        if (result.value) {
                            axios.delete('/api/content-management/projects/contact/chat-history/'+that.attr('chat-id'))
                            .then(function (response) {
                                swalWithBootstrapButtons.fire(
                                'Deleted!',
                                'Your file has been deleted.',
                                'success'
                                );
                                location.reload();
                            })
                            .catch(function (error) {
                                console.log(error);
                            });
                        }
                    })
                });
            });

            $("#update-chat-history").on("click", function(e){
                e.preventDefault();
                axios.post('/api/content-management/projects/contact/chat-history/'+$(this).attr('chat-id'), {
                    contact_person: $("input[name=contact_person].chat-history").val(),
                    medium_id: $("select[name=medium_id].chat-history").val(),
                    interest_level_id: $("select[name=interest_level_id].chat-history").val(),
                    description: CKEDITOR.instances['description'].getData(),
                    next_follow_up : $("input[name=next_follow_up].chat-history").val(),
                })
                .then(function (response) {
                    location.reload();
                    // $('#validation-errors').html('');
                    // $('#chat-history').modal('hide');
                })
                .catch(function (error) {
                    console.log(error);$('#validation-errors').html('');
                    let errors='';
                    $.each(error.response.data, function(key,value) {
                        errors+='<li>'+value[0]+'</li>';
                    });
                    $('#validation-errors').append('<div class="alert alert-danger"><ul>'+errors+'</ul></div');
                });
            });

            
        });
    </script>
@endsection

