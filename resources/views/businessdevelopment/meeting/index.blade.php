@extends('layout.navbar')

@section('contents')

<section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Meetings</h3>
                        <div class="btn-group pull-right">
                           <a  href="{{route('create-meeting')}}" > <button type="button" class="btn btn-default btn-sm">
                                Create Meeting 
                            </button></a>
                        </div>
                    </div>
                    <div class="box-body">
                        <table class="table" id="meetings_table">
                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Time</th>
                                    <th>Date</th>
                                    <th>Venue</th>
                                    <th>Estimated Meeting Period</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>     
@endsection

@section('scripts')
    <script>
        $(document).ready( function () {
            let Otable = $("#meetings_table");
            Otable.DataTable().destroy();  
            let table = Otable.DataTable({
                "processing": true,
                "serverSide": false,
                "pageLength": 50,
                ajax: "{{ route('meeting-index') }}",
                columns: [
                    { data: 'title' },
                    { data: 'time' },
                    { data: 'date' },
                    { data: 'venue' },
                    { data: 'estimated_meeting_period' },
                    { data: 'actions' },
                ],

                order: [ [2, 'desc'] ], 
                "columnDefs": [
                    {
                        "searchable": false,
                        "targets": [3]
                    }, 
                    {
                        "orderable": false,
                        "targets": [3]
                    },
                ],
                select:true,
                dom: 'Bfrtip',
            });
        });
        
    </script>
@endsection