<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Country;
use App\Project;
use App\IndividualContact;
use App\BusinessContact;
use App\ContactsStatus;
use DB;
use Yajra\Datatables\Datatables;
use App\MarkettingIndividualContact;
use App\MarkettingBusinessContact;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class MarkettingIndividualContactController extends Controller
{
    public function getIndividualContacts($project_id)
    {
        $contacts = MarkettingIndividualContact::where([
            ['project_id', $project_id]
        ])
            ->orderBy('created_at', 'DESC')
            ->with('status', 'country')
            ->get();
        return Datatables::of($contacts)
         ->addColumn('checkbox', function ($contacts)
         {
            $checkbox='<input type="checkbox" class="ind_select" contact-id='.$contacts->id.'><br>';
            return $checkbox;
         })
         ->addColumn('actions', function ($contacts)
         {
            $buttons = '<div class="dropdown">
                            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Actions
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li><a href="#" disabled class="ind-actions view-ind" contact_id='.$contacts->id.'><i class="fa fa-eye" aria-hidden="true"></i> View</a></li>
                                '.($contacts->accept_status == 0 ? '
                                    <li role="presentation" class="divider"></li>
                                    <li>
                                        <a href="#" disabled class="verify-ind-contact" contact_id='.$contacts->id.' contact_type="individual" accept_status="1">
                                            <i class="fa fa-key" aria-hidden="true">
                                            </i>
                                            Activate
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" disabled class="verify-ind-contact" contact_id='.$contacts->id.' contact_type="individual" accept_status="2">
                                            <i class="fa fa-ban" aria-hidden="true">
                                            </i>
                                            Reject
                                        </a>
                                    </li>
                                ': '').'
                            </ul>
                        </div>';
            return $buttons;
         })
         ->rawColumns(['checkbox', 'actions'])
         ->make(true);
    }

    public function createContact(Request $request, $slug)
    {
        $this->validate($request, [
            'name' => 'required||max:255',
            'address' => 'required||max:255',
            'country_id' => 'required',
            'status_id' => 'required',
            'email' => 'required||email',
            'phone_number' => 'required',
        ]);

        DB::beginTransaction();
        try{
            $project = Project::where('slug', $slug)->first();

            $contact = MarkettingIndividualContact::create([
                'name' => $request->name,
                'phone_number' => $request->phone_number,
                'email' => $request->email,
                'address' => $request->address,
                'country_id' => $request->country_id,
                'status_id' => $request->status_id,
                'remarks' => $request->remarks,
                'project_id' => $project->id,
            ]);

            
            $file = $request->file('file');
            if ($file) {
                $extension = $file->getClientOriginalExtension();
                $filename = time().'.'.$extension;
                $url = 'documents/projects/'.$project->id.'/individual/'.$filename;
                $original_filename = $file->getClientOriginalName();
                Storage::disk('public')->put($url,  File::get($file));
                $contact->image()
                ->create(
                    [   
                        "url" => $url,
                        'original_filename' => $original_filename
                    ]
                );
            }
            DB::commit();
            return redirect()->back()->with('success', 'Form Submitted Sucessfully.'); 

        }catch(\Exception $errors){
            DB::rollback();
            return back()->withErrors($errors->getMessage());
        }
    }

    public function showContact($contact_id)
    {
        $contact_type = 'individual';
        $contact = MarkettingIndividualContact::where('id', $contact_id)->with('image')->first();
        $compact = ['contact_type', 'contact'];
        return view('businessdevelopment.marketting.contact.view.contact', compact($compact));
    }

    public function filterIndividualContact(Request $request)
    {
        $name = $request->input('name');
        $email = $request->input('email');
        // $accept_status = $request->input('accept_status');
        $address = $request->input('address');
        $phone_number = $request->input('phone_number');
        $status_id = $request->input('status_id');
        $joined_from = $request->input('joined_from');
        $joined_till = $request->input('joined_till');
        $contacts = MarkettingIndividualContact::when($name, function ($query) use ($name) {
                return $query->where('name', 'like', "%$name%");
            })
            ->when($email, function ($query) use ($email) {
                return $query->where('email', $email);
            })
            // ->when($accept_status, function ($query) use ($accept_status) {
            //     return $query->where('accept_status', $accept_status);
            // })
            ->when($address, function ($query) use ($address) {
                return $query->where('address', "like", "%$address%");
            })
            ->when($phone_number, function ($query) use ($phone_number) {
                return $query->where('phone_number', $phone_number);
            })
            ->when($status_id, function ($query) use ($status_id) {
                if ($status_id != 'all-status') {
                    return $query->where('status_id', $status_id);
                } else {
                    return $query;
                }
            })
            ->when($joined_from, function ($query) use ($joined_from) {
                return $query->whereDate('created_at', '>=', $joined_from);
            })
            ->when($joined_till, function ($query) use ($joined_till) {
                return $query->whereDate('created_at', '<=', $joined_till);
            })
            ->where('project_id', $request->input('project_id'))
            ->orderBy('created_at', 'DESC')
            ->with('status', 'country')
            ->get();
            
        return Datatables::of($contacts)
            ->addColumn('checkbox', function ($contacts)
            {
            $checkbox='<input type="checkbox" class="ind_select" contact-id='.$contacts->id.'><br>';
            return $checkbox;
            })
            ->addColumn('actions', function ($contacts)
            {
                $buttons = '<div class="dropdown">
                                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Actions
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li><a href="#" disabled class="ind-actions view-ind" contact_id='.$contacts->id.'><i class="fa fa-eye" aria-hidden="true"></i> View</a></li>
                                    '.($contacts->accept_status == 0 ? '
                                        <li role="presentation" class="divider"></li>
                                        <li>
                                            <a href="#" disabled class="verify-ind-contact" contact_id='.$contacts->id.' contact_type="individual" accept_status="1">
                                                <i class="fa fa-key" aria-hidden="true">
                                                </i>
                                                Activate
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" disabled class="verify-ind-contact" contact_id='.$contacts->id.' contact_type="individual" accept_status="2">
                                                <i class="fa fa-ban" aria-hidden="true">
                                                </i>
                                                Reject
                                            </a>
                                        </li>
                                    ': '').'
                                </ul>
                            </div>';
                return $buttons;
            })
            ->rawColumns(['checkbox', 'actions'])
            ->make(true);
    }

    public function autoComplete(Request $request)
    {
        $contacts = MarkettingIndividualContact::where([
            ['project_id', $request->project_id],
            ['name', 'like', "%$request->name%"]
        ])->pluck('name');
        return response()->json($contacts, 200);
    }
}
