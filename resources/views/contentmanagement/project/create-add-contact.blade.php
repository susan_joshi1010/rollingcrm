@extends('layout.navbar')

@section('contents')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Create Project | {{$contact_type}} contact </h3>
                        <a href="{{url('/content-management/projects/create/'.$project_id)}}" class="btn btn-primary btn-sm pull-right">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back 
                        </a>
                    </div>
                    <div class="box-body">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if (\Session::has('success'))
                            <div class="alert alert-success">
                                <ul>
                                    <li>{!! \Session::get('success') !!}</li>
                                </ul>
                            </div>
                        @endif
                        @if($contact_type == 'individual')
                            {!! Form::open(['route' => ['update-project-individual', $project_id, 0], 'method' => 'PUT', 'enctype' => 'multipart/form-data']) !!}
                        @elseif ($contact_type == 'business')
                            {!! Form::open(['route' => ['update-project-business', $project_id, 0], 'method' => 'PUT', 'enctype' => 'multipart/form-data']) !!}
                        @endif
                        <div class="row">
                            @if($contact_type == 'individual')
                                @include('contentmanagement.contact.individual-contact-form')
                            @elseif ($contact_type == 'business')
                                @include('contentmanagement.contact.business-contact-form')
                            @endif
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary pull-right">Submit</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>   
        </div>
    </section>
@endsection
@section('scripts')
<script>
    $(function () {
        CKEDITOR.replace( 'objective' );
    })
</script>
@endsection