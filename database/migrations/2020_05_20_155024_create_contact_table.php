<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('individual_contact', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('marketing_individual_contact_id')->nullable();
            $table->string('name');
            $table->string('phone_number');
            $table->string('email')->unique();
            $table->string('address');
            $table->integer('country_id')->unsigned();
            $table->integer('project_id')->unsigned();
            $table->integer('status_id')->unsigned();
            $table->foreign('country_id')->references('id')->on('country');
            $table->foreign('project_id')->references('id')->on('project');
            $table->foreign('status_id')->references('id')->on('contact_status');
            $table->longText('remarks');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('individual_contact');
    }
}
