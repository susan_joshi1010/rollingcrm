<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarkettingBusinessContactTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marketting_business_contact', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('accept_status')->default(0);
            $table->string('organization_name');
            $table->string('phone_number');
            $table->string('point_of_contact');
            $table->string('point_of_contact_no');
            $table->string('email')->unique();
            $table->string('address');
            $table->integer('project_id')->unsigned();
            $table->integer('country_id')->unsigned();
            $table->integer('status_id')->unsigned();
            $table->foreign('project_id')->references('id')->on('project');
            $table->foreign('country_id')->references('id')->on('country');
            $table->foreign('status_id')->references('id')->on('contact_status');
            $table->longText('remarks');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marketting_business_contact');
    }
}
