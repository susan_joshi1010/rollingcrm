<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarkettingIndividualContact extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'marketting_individual_contact';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    public function status()
    {
        return $this->belongsTo('App\ContactsStatus');
    }

    public function country()
    {
        return $this->belongsTo('App\Country');
    }

    public function project()
    {
        return $this->belongsTo('App\Project');
    }

    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }
}
