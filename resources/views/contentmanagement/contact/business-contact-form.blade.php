<div class="form-group col-md-4">
    <label>Organization Name</label>
    {!! Form::text('organization_name', null, ['class' => 'form-control', 'placeholder' => 'Enter name']) !!}        
</div>
<div class="form-group col-md-4">
    <label>Phone Number</label>
    {!! Form::number('phone_number', null, ['class' => 'form-control', 'placeholder' => 'Enter Phone Number']) !!}  
</div>
<div class="form-group col-md-4">
    <label>Point of Contact</label>
    {!! Form::text('point_of_contact', null, ['class' => 'form-control', 'placeholder' => 'Enter Point of contact']) !!}        
</div>
<div class="form-group col-md-4">
    <label>Point of Contact Number</label>
    {!! Form::number('point_of_contact_no', null, ['class' => 'form-control', 'placeholder' => 'Enter Point of Contact Number']) !!}  
</div>
<div class="form-group col-md-4">
    <label>Email</label>
    {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Enter email']) !!}  
</div>
<div class="form-group col-md-4">
    <label>Address</label>
    {!! Form::text('address', null, ['class' => 'form-control', 'placeholder' => 'Enter Address']) !!}  
</div>
<div class="form-group col-md-4">
    <label>Country</label>
    {!! Form::select('country_id', $countries, null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-md-4">
    <label for="file">Documents 
    @if(isset($contact) )
        {{ $contact->image != null? $contact->image->original_filename:''}}
    @endif
    </label>
    <input type="file" id="file" name="file" class="form-control-file">
</div>
<div class="form-group col-md-4">
    <label>Status</label>
    {!! Form::select('status_id', $contact_status, null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-md-12">
    <label>Remarks</label>
    {!! Form::textarea('remarks', null, ['class' => 'form-control', 'rows' => '2',  "placeholder" => "Add Remarks"]) !!}
</div>