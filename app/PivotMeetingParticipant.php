<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PivotMeetingParticipant extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pivot_meeting_participant';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];
}