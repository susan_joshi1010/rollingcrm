<?php

namespace App\Http\Controllers;

use App\Meeting;
use App\MeetingParticipant;
use App\User;
use App\PivotMeetingParticipant;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Yajra\Datatables\Datatables;
use App\Mail\Email;
use Illuminate\Support\Facades\Mail;

class MeetingController extends Controller
{
   
    public function index(Request $request)
    {
        if($request->ajax()){
            $meeting = Meeting::latest()->get();
            return Datatables::of($meeting)
                    ->addIndexColumn()
                    ->addColumn('actions',function($meeting){
                        $buttons='<div class="dropdown">
                                        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Actions
                                            <span class="caret"></span></button>
                                             <ul class="dropdown-menu">
                                                <li><a href="/business-development/meeting/show/'.$meeting->id.'" class="view-meet"><i class="fa fa-eye" aria-hidden="true"></i> View</a></li>
                                                <li><a href="/business-development/meeting/edit/'.$meeting->id.'" class="update-meet"><i class="fa fa-refresh" aria-hidden="true"></i> Update</a></li>
                                            </ul>
                                    </div>';
                        return $buttons;
                    })
                    ->rawColumns(['actions'])
                    ->make(true);
        }

        return view('businessdevelopment.meeting.index');
    }

    public function create()
    {
        $user = User::pluck('name', 'id');
        return view('businessdevelopment.meeting.create',compact('user'));
    }

   
    public function store(Request $request)
    {
        // dd($request->all());
        $this->validate($request,[
            'title'=>'required||max:255',
            'time'=>'required',
            'date'=>'required',
            'venue'=>'required||max:255',
            'agenda'=>'required',
            'estimated_meeting_period'=>'required',
        ]);

        DB::beginTransaction();
        try{
            $meeting = Meeting::create([
                'title' => $request->title,
                'time' => $request->time,
                'date' => $request->date,
                'venue' => $request->venue,
                'agenda' => $request->agenda,
                'estimated_meeting_period' => $request->estimated_meeting_period
            ]);

            $participant_id = [];
            foreach ($request->new as $key => $participant) {
                $participant = MeetingParticipant::create([
                    'name' => $participant['name'],
                    'email' => $participant['email'],
                    'phone_number' => $participant['phone_number'],
                    'position' => $participant['position'],
                    'organization_name' => $participant['organization_name'],
                ]);
                array_push($participant_id, $participant->id);
                Mail::to($participant['email'])->send(new Email($meeting));
            }
            
            foreach ($request->user_id as $key => $id) {
                $user = User::where('id', $id)->first();
                $participant = MeetingParticipant::create([
                    'name' => $user->name,
                    'email' => $user->email,
                    'phone_number' => "9841156449",
                    'position' => "Manager",
                    'organization_name' => "Rolling Plans",
                ]);
                array_push($participant_id, $participant->id);
                Mail::to($user->email)->send(new Email($meeting));
            }

            foreach ($participant_id as $key => $p_id) {
                PivotMeetingParticipant::create([
                    "meeting_id" => $meeting->id,
                    "meeting_participant_id" => $p_id
                ]);
            }

            DB::commit();
            return redirect()->back()->with('success', 'Meeting created sucessfully.'); 

        }catch(\Exception $errors){
            DB::rollback();
            return back()->withErrors($errors->getMessage());
        }
    }

    
    public function show($id)
    {
        $meeting = Meeting::where('id', $id)->first();
        return view('businessdevelopment.meeting.show', compact('meeting'));
    }

    
    public function edit($id)
    {
        $meeting = Meeting::where('id', $id)->first();
        return view('businessdevelopment.meeting.update',compact('meeting'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title'=>'required||max:255',
            'time'=>'required',
            'date'=>'required',
            'venue'=>'required||max:255',
            'agenda'=>'required',
            'estimated_meeting_period'=>'required',
        ]);

        $data = [
            'title' => $request->title,
            'time' => $request->time,
            'date' => $request->date,
            'venue' => $request->venue,
            'agenda' => $request->agenda,
            'estimated_meeting_period' => $request->estimated_meeting_period,
        ];
        
        $meeting = Meeting::find($id)->update($data);
        return redirect()->back()->with('success', 'Successfully Contract Updated.'); 
    }

   
    // public function delete($id) 
    // { 
    //     Meeting::where('id', $id)->delete();
    //     return response()->json(["message" => "sucessfully removed"], 200);
    // }
}
