<?php
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\Email;
use Illuminate\Support\Facades\Mail;

 
class MailController extends Controller
{

    public function compose()
    {
        return view('mails.compose');
    }


    public function send(Request $request)
    {
     $this->validate($request, [
      'email'  =>  'required|email',
      'subject'     =>  'required',
      'message' =>  'required'
     ]);

        $data = array(
            'subject'      =>  $request->subject,
            'message'   =>   $request->message
        );

     Mail::to($request->email)->send(new Email($data));
     return back()->with('success', 'Your mail has been sent!');

    }
}