<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Country;
use App\Project;
use App\Image;
use App\IndividualContact;
use App\BusinessContact;
use App\Medium;
use App\InterestLevel;
use App\ContactsStatus;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use DB;
use Yajra\Datatables\Datatables;

class IndividualContactsController extends Controller
{
   public function store(Request $request)
    {
        $this->validate($request, [
            'project_name' => 'required||max:255',
            'objective' => 'required',
            'project_coordinator' => 'required||max:255',
            'name' => 'required||max:255',
            'address' => 'required||max:255',
            'country_id' => 'required',
            'status_id' => 'required',
            'email' => 'required||email',
            'phone_number' => 'required',
        ]);

        DB::beginTransaction();
        try{
            $project = Project::create([
                'project_name' => $request->project_name,
                'project_coordinator' => $request->project_coordinator,
                'objective' => $request->objective,
            ]);

            $documents = $request->file('document');
            if ($documents) {
                foreach ($documents as $key => $document) {
                    $extension = $document->getClientOriginalExtension();
                    $documentname = $key.time().'.'.$extension;
                    $url = 'documents/projects/'.$project->id.'/'.$documentname;
                    $original_filename = $document->getClientOriginalName();
                    Storage::disk('public')->put($url,  File::get($document));
                    $project->image()
                    ->create(
                        [   
                            "url" => $url,
                            'original_filename' => $original_filename
                            ]
                        );
                }
            }

            $contact = IndividualContact::create([
                'name' => $request->name,
                'phone_number' => $request->phone_number,
                'email' => $request->email,
                'address' => $request->address,
                'country_id' => $request->country_id,
                'status_id' => $request->status_id,
                'remarks' => $request->remarks,
                'project_id' => $project->id,
            ]);

            
            $file = $request->file('file');
            if ($file) {
                $extension = $file->getClientOriginalExtension();
                $filename = time().'.'.$extension;
                $url = 'documents/projects/'.$project->id.'/individual/'.$filename;
                $original_filename = $file->getClientOriginalName();
                Storage::disk('public')->put($url,  File::get($file));
                $contact->image()
                    ->create(
                        [   
                            "url" => $url,
                            'original_filename' => $original_filename
                        ]
                    );
            }
            DB::commit();
            return redirect()->back()->with('success', 'Project | Individual Contact created sucessfully.'); 

        }catch(\Exception $errors){
            DB::rollback();
            return back()->withErrors($errors->getMessage());
        }
    }

    public function getIndividualContacts($project_id)
    {
        $contacts = IndividualContact::where('project_id', $project_id)
            ->orderBy('created_at', 'DESC')
            ->with('status', 'country')
            ->get();
        return Datatables::of($contacts)
         ->addColumn('checkbox', function ($contacts)
         {
            $checkbox='<input type="checkbox" class="ind_select" contact-id='.$contacts->id.'><br>';
            return $checkbox;
         })->addColumn('actions', function ($contacts)
         {
            $buttons = '<div class="dropdown">
                            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Actions
                            <span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="/content-management/projects/'.$contacts->project_id.'/contact/individual/'.$contacts->id.'" class="ind-actions view-ind" contact-id="'.$contacts->id.'"><i class="fa fa-eye" aria-hidden="true"></i> View</a></li>
                                <li><a href="#" disabled class="ind-actions update-ind" contact-id="'.$contacts->id.'"><i class="fa fa-refresh" aria-hidden="true"></i> Update</a></li>
                                <li role="presentation" class="divider"></li>
                                <li role="presentation"><a href=""  data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#add-chat-history" role="menuitem" tabindex="-1" class="ind-actions chat-history-link" contact-type="Individual" contact-id="'.$contacts->id.'"><i class="fa fa-plus" aria-hidden="true"></i> Add Chat History</a></li>
                            </ul>
                        </div>';
            return $buttons;
         })
         ->rawColumns(['checkbox', 'actions'])
         ->make(true);
    }

    public function showContact($project_id, $contact_id) {
        $contact = IndividualContact::where('id' ,$contact_id)->with('country', 'status')->first();
        $contact_type = 'individual';
        $medium = Medium::pluck('medium', 'id');
        $interest_level = InterestLevel::pluck('interest_level', 'id');
        $compact = ['contact', 'contact_type', 'contact_id', 'medium', 'interest_level'];
        return view('contentmanagement.contact.view.contact', compact($compact));
    }

    public function updateProject(Request $request, $project_id, $contact_id) {
        // dd($request->all(), $project_id, $contact_id);
        $this->validate($request, [
            'name' => 'required||max:255',
            'address' => 'required||max:255',
            'country_id' => 'required',
            'status_id' => 'required',
            'email' => 'required||email',
            'phone_number' => 'required'
        ]);

        DB::beginTransaction();
        try{

            $data = [
                'name' => $request->name,
                'phone_number' => $request->phone_number,
                'email' => $request->email,
                'address' => $request->address,
                'country_id' => $request->country_id,
                'status_id' => $request->status_id,
                'remarks' => $request->remarks
            ];

            if ($contact_id != 0) {
                $contact = IndividualContact::find($contact_id)->update($data);
                $contact = IndividualContact::find($contact_id);
            }else{
                $data['project_id'] = $project_id;
                $contact = IndividualContact::create($data);
            }

            $file = $request->file('file');
            if ($file) {
                $extension = $file->getClientOriginalExtension();
                $filename = time().'.'.$extension;
                $original_filename = $file->getClientOriginalName();
                $url = 'documents/projects/'.$project_id.'/individual/'.$filename;
                Storage::disk('public')->put($url,  File::get($file));
                $contact->image()
                    ->create(
                        [   
                            "url" => $url,
                            'original_filename' => $original_filename
                        ]
                    );
            }
            DB::commit();
            return redirect()->back()->with('success', 'Successfully Contract Updated.'); 

        }catch(\Exception $errors){
            DB::rollback();
            return back()->withErrors($errors->getMessage());
        }
    }

    public function filterIndividualContact(Request $request)
    {
        // dd($request->all(), $request->name);
        $name = $request->input('name');
        $email = $request->input('email');
        $address = $request->input('address');
        $phone_number = $request->input('phone_number');
        $status_id = $request->input('status_id');
        $joined_from = $request->input('joined_from');
        $joined_till = $request->input('joined_till');
        $contacts = IndividualContact::when($name, function ($query) use ($name) {
                return $query->where('name', 'like', "%$name%");
            })
            ->when($email, function ($query) use ($email) {
                return $query->where('email', $email);
            })
            ->when($address, function ($query) use ($address) {
                return $query->where('address', "like", "%$address%");
            })
            ->when($phone_number, function ($query) use ($phone_number) {
                return $query->where('phone_number', $phone_number);
            })
            ->when($status_id, function ($query) use ($status_id) {
                if ($status_id != 'all-status') {
                    return $query->where('status_id', $status_id);
                } else {
                    return $query;
                }
            })
            ->when($joined_from, function ($query) use ($joined_from) {
                return $query->whereDate('created_at', '>=', $joined_from);
            })
            ->when($joined_till, function ($query) use ($joined_till) {
                return $query->whereDate('created_at', '<=', $joined_till);
            })
            ->where('project_id', $request->input('project_id'))
            ->orderBy('created_at', 'DESC')
            ->with('status', 'country')
            ->get();
            
        return Datatables::of($contacts)
            ->addColumn('checkbox', function ($contacts)
            {
                $checkbox='<input type="checkbox" class="ind_select" contact-id='.$contacts->id.'><br>';
                return $checkbox;
            })->addColumn('actions', function ($contacts)
            {
                $buttons = '<div class="dropdown">
                            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Actions
                            <span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="/content-management/projects/'.$contacts->project_id.'/contact/individual/'.$contacts->id.'" class="ind-actions view-ind" contact-id="'.$contacts->id.'"><i class="fa fa-eye" aria-hidden="true"></i> View</a></li>
                                <li><a href="#" disabled class="ind-actions update-ind" contact-id="'.$contacts->id.'"><i class="fa fa-refresh" aria-hidden="true"></i> Update</a></li>
                                <li role="presentation" class="divider"></li>
                                <li role="presentation"><a href=""  data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#add-chat-history" role="menuitem" tabindex="-1" class="ind-actions chat-history-link" contact-type="Individual" contact-id="'.$contacts->id.'"><i class="fa fa-plus" aria-hidden="true"></i> Add Chat History</a></li>
                            </ul>
                        </div>';
                return $buttons;
            })
            ->rawColumns(['checkbox', 'actions'])
            ->make(true);
    }

    public function autoComplete(Request $request)
    {
        $contacts = IndividualContact::where([
            ['project_id', $request->project_id],
            ['name', 'like', "%$request->name%"]
        ])->pluck('name');
        return response()->json($contacts, 200);
    }

}
