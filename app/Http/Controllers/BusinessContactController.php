<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Country;
use App\Project;
use App\Image;
use App\IndividualContact;
use App\BusinessContact;
use App\ContactsStatus;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use DB;
use Yajra\Datatables\Datatables;
use App\Medium;
use App\InterestLevel;

class BusinessContactController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request, [
            'project_name' => 'required||max:255',
            'project_coordinator' => 'required||max:255',
            'objective' => 'required',
            'organization_name' => 'required||max:255',
            'email' => 'required||email',
            'point_of_contact' => 'required',
            'point_of_contact_no' => 'required',
            'phone_number' => 'required',
            'status_id' => 'required',
            'address' => 'required',
        ]);

        DB::beginTransaction();
        try{
            $project = Project::create([
               'project_name' => $request->project_name,
               'project_coordinator' => $request->project_coordinator,
               'objective' => $request->objective,
            ]);

            $documents = $request->file('document');
            if ($documents) {
                foreach ($documents as $key => $document) {
                    $extension = $document->getClientOriginalExtension();
                    $documentname = $key.time().'.'.$extension;
                    $url = 'documents/projects/'.$project->id.'/'.$documentname;
                    $original_filename = $document->getClientOriginalName();
                    Storage::disk('public')->put($url,  File::get($document));
                    $project->image()
                    ->create(
                        [   
                            "url" => $url,
                            'original_filename' => $original_filename
                            ]
                        );
                }
            }

            $business_contact = BusinessContact::create([
                'organization_name' => $request->organization_name,
                'phone_number' => $request->phone_number,
                'email' => $request->email,
                'point_of_contact' => $request->point_of_contact,
                'point_of_contact_no' => $request->point_of_contact_no,
                'address' => $request->address,
                'project_id' => $project->id,
                'country_id' => $request->country_id,
                'status_id' => $request->status_id,
                'remarks' => $request->remarks,
            ]);

            $file = $request->file('file');
            if ($file) {
                $extension = $file->getClientOriginalExtension();
                $filename = time().'.'.$extension;
                $original_filename = $file->getClientOriginalName();
                $url = 'documents/projects/'.$project->id.'/business/'.$filename;
                Storage::disk('public')->put($url,  File::get($file));
                $business_contact->image()
                    ->create(
                        [   
                            "url" => $url,
                            'original_filename' => $original_filename
                        ]
                    );
            }
            DB::commit();
            return redirect()->back()->with('success', 'Project | Business Contact created sucessfully.'); 

        }catch(\Exception $errors){
            DB::rollback();
            return back()->withErrors($errors->getMessage());
        }
    }

    public function getBusinessContacts($project_id)
    {
        $business_contacts = BusinessContact::where('project_id', $project_id)
        ->orderBy('created_at', 'DESC')
        ->with('status', 'country')
        ->get();
        $contact_status = ContactsStatus::pluck('title', 'id');
        return Datatables::of($business_contacts)
         ->addColumn('checkbox', function ($business_contacts)
         {
            $checkbox='<input type="checkbox" class="bus_select" contact-id='.$business_contacts->id.'><br>';
            return $checkbox;
         })->addColumn('actions', function ($business_contacts)
         {
            $buttons = '<div class="dropdown">
                            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Actions
                                <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="/content-management/projects/'.$business_contacts->project_id.'/contact/business/'.$business_contacts->id.'" class="bus-actions view-bus" contact-id="'.$business_contacts->id.'"><i class="fa fa-eye" aria-hidden="true"></i> View</a></li>
                                        <li><a href="#" disabled class="bus-actions update-bus" contact-id="'.$business_contacts->id.'"><i class="fa fa-refresh" aria-hidden="true"></i> Update</a></li>
                                        <li role="presentation" class="divider"></li>
                                        <li role="presentation"><a href=""  data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#add-chat-history" role="menuitem" tabindex="-1" class="bus-actions chat-history-link" contact-type="Business" contact-id="'.$business_contacts->id.'"><i class="fa fa-plus" aria-hidden="true"></i> Add Chat History</a></li>
                                    </ul>
                        </div>';
             return $buttons;
         })
         ->rawColumns(['checkbox', 'actions'])
         ->make(true);
    }

    public function showContact($project_id, $contact_id) {
        $contact = BusinessContact::where('id' ,$contact_id)->with('country', 'status')->first();
        $contact_type = 'business';
        $medium = Medium::pluck('medium', 'id');
        $interest_level = InterestLevel::pluck('interest_level', 'id');
        $compact = ['contact', 'contact_type', 'contact_id', 'medium', 'interest_level'];
        return view('contentmanagement.contact.view.contact', compact($compact));
    }

    public function updateProject(Request $request, $project_id, $contact_id) {
        // dd($request->all(), $project_id, $contact_id);
        $this->validate($request, [
            'organization_name' => 'required||max:255',
            'email' => 'required||email',
            'point_of_contact' => 'required',
            'point_of_contact_no' => 'required',
            'phone_number' => 'required',
            'status_id' => 'required',
            'address' => 'required',
        ]);

        DB::beginTransaction();
        try{
            $data = [
                'organization_name' => $request->organization_name,
                'phone_number' => $request->phone_number,
                'email' => $request->email,
                'point_of_contact' => $request->point_of_contact,
                'point_of_contact_no' => $request->point_of_contact_no,
                'address' => $request->address,
                'country_id' => $request->country_id,
                'status_id' => $request->status_id,
                'remarks' => $request->remarks,
            ];

            if ($contact_id != 0) {
                $contact = BusinessContact::find($contact_id)->update($data);
                $contact = BusinessContact::find($contact_id);
            }else{
                $data['project_id'] = $project_id;
                $contact = BusinessContact::create($data);
            }

            $file = $request->file('file');
            if ($file) {
                $extension = $file->getClientOriginalExtension();
                $filename = time().'.'.$extension;
                $original_filename = $file->getClientOriginalName();
                $url = 'documents/projects/'.$project_id.'/business/'.$filename;
                Storage::disk('public')->put($url,  File::get($file));
                $contact->image()
                    ->create(
                        [   
                            "url" => $url,
                            'original_filename' => $original_filename
                        ]
                    );
            }
            DB::commit();
        return redirect()->back()->with('success', 'Successfully Contract Updated.'); 

        }catch(\Exception $errors){
            DB::rollback();
            return back()->withErrors($errors->getMessage());
        }
    }

    public function filterBusinessContact(Request $request)
    {
        // dd($request->all(), $request->name);
        $organization_name = $request->input('organization_name');
        $point_of_contact = $request->input('point_of_contact');
        $email = $request->input('email');
        $address = $request->input('address');
        $phone_number = $request->input('phone_number');
        $status_id = $request->input('status_id');
        $joined_from = $request->input('joined_from');
        $joined_till = $request->input('joined_till');
        $business_contacts = BusinessContact::when($organization_name, function ($query) use ($organization_name) {
                return $query->where('organization_name', 'like', "%$organization_name%");
            })
            ->when($email, function ($query) use ($email) {
                return $query->where('email', $email);
            })
            ->when($point_of_contact, function ($query) use ($point_of_contact) {
                return $query->where('point_of_contact', 'like', "%$point_of_contact%");
            })
            ->when($address, function ($query) use ($address) {
                return $query->where('address', "like", "%$address%");
            })
            ->when($phone_number, function ($query) use ($phone_number) {
                return $query->where('phone_number', $phone_number);
            })
            ->when($status_id, function ($query) use ($status_id) {
                if ($status_id != 'all-status') {
                    return $query->where('status_id', $status_id);
                } else {
                    return $query;
                }
            })
            ->when($joined_from, function ($query) use ($joined_from) {
                return $query->whereDate('created_at', '>=', $joined_from);
            })
            ->when($joined_till, function ($query) use ($joined_till) {
                return $query->whereDate('created_at', '<=', $joined_till);
            })
            ->where('project_id', $request->input('project_id'))
            ->orderBy('created_at', 'DESC')
            ->with('status', 'country')
            ->get();
            
        return Datatables::of($business_contacts)
            ->addColumn('checkbox', function ($business_contacts)
            {
                $checkbox='<input type="checkbox" class="bus_select" contact-id='.$business_contacts->id.'><br>';
                return $checkbox;
            })->addColumn('actions', function ($business_contacts)
            {
                $buttons = '<div class="dropdown">
                                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Actions
                                    <span class="caret"></span></button>
                                        <ul class="dropdown-menu">
                                            <li><a href="/content-management/projects/'.$business_contacts->project_id.'/contact/business/'.$business_contacts->id.'" class="bus-actions view-bus" contact-id="'.$business_contacts->id.'"><i class="fa fa-eye" aria-hidden="true"></i> View</a></li>
                                            <li><a href="#" disabled class="bus-actions update-bus" contact-id="'.$business_contacts->id.'"><i class="fa fa-refresh" aria-hidden="true"></i> Update</a></li>
                                            <li role="presentation" class="divider"></li>
                                            <li role="presentation"><a href=""  data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#add-chat-history" role="menuitem" tabindex="-1" class="bus-actions chat-history-link" contact-type="Business" contact-id="'.$business_contacts->id.'"><i class="fa fa-plus" aria-hidden="true"></i> Add Chat History</a></li>
                                        </ul>
                            </div>';
                return $buttons;
            })
            ->rawColumns(['checkbox', 'actions'])
            ->make(true);
    }

    public function autoComplete(Request $request)
    {
        // dd($request->all(), $organization_name);
        $organization_name = $request->input('organization_name');
        $point_of_contact = $request->input('point_of_contact');
        $email = $request->input('email');
        $address = $request->input('address');
        $phone_number = $request->input('phone_number');
        
        $contacts = BusinessContact::where('project_id', $request->project_id)
            ->when($organization_name, function ($query) use ($organization_name) {
                return $query->where('organization_name', 'like', "%$organization_name%")->pluck("organization_name");
            })
            ->when($email, function ($query) use ($email) {
                return $query->where('email', "like" , "%$email%")->pluck("email");
            })
            ->when($point_of_contact, function ($query) use ($point_of_contact) {
                return $query->where('point_of_contact', 'like', "%$point_of_contact%")->pluck("point_of_contact");
            })
            ->when($address, function ($query) use ($address) {
                return $query->where('address', "like", "%$address%")->pluck("address");
            })
            ->when($phone_number, function ($query) use ($phone_number) {
                return $query->where('phone_number', "like", "%$phone_number%")->pluck("phone_number");
            });
        return response()->json($contacts, 200);
    }
}
