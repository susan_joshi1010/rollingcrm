@extends('layout.navbar')

@section('contents')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Projects</h3>
                        <div class="btn-group pull-right">
                            <a href="{{route('marketting-create-project')}}" class="btn btn-default btn-sm">
                                Create Project
                            </a>
                        </div>
                    </div>
                    <div class="box-body">
                        <table class="table" id="projects_table">
                            <thead>
                                <tr>
                                    <th>Project Name</th>
                                    <th>Project Coordinator</th>
                                    <th>Created Date</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script>
        $(document).ready( function () {
            let Otable = $("#projects_table");
            Otable.DataTable().destroy();  
            let table = Otable.DataTable({
                "processing": true,
                "serverSide": false,
                "pageLength": 50,
                ajax: {
                    "url" : '/api/business-development/marketting/projects',
                },
                columns: [
                    { data: 'project_name' },
                    { data: 'project_coordinator' },
                    { data: 'created_at' },
                    { data: 'actions' },
                ],
                order: [ [2, 'desc'] ], 
                "columnDefs": [
                    {
                        "searchable": false,
                        "targets": [3]
                    }, 
                    {
                        "orderable": false,
                        "targets": [3]
                    },
                ],
                select:true,
                dom: 'Bfrtip',
            });
            table.on('draw.dt', function(){
                $(".view").on('click',function(){
                    window.location.href = '/business-development/marketting/project/'+$(this).attr('project-id');
                });
            });
        });
    </script>
@endsection