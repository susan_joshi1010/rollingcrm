
<p>Hi Sir/Madam,</p>
<p>Subject:{{ $data['subject'] }}
    <p>Meeting Agenda</p>
   <p> Meeting purpose:</p>
   <p> For the content marketing team to develop our content strategy action plan for Q4 2019 and Q1 2020.</p>

   <p> Before the meeting:</p>
   <p> All participants must review the Keyword Analysis and Q2 & Q3 Content Performance reports (attached to invite) prior to the meeting.</p>

   <p> Agenda:</p>
   <p> Q2 and Q3 performance discussion (10 minutes)</p>
   <p> Review topics requested by the sales team (10 minutes)</p>
   <p> Content gap analysis (15 minutes)</p>
   <p> Content brainstorm (15 minutes)</p>
   <p> General Q&A (5 minutes)</p>
   <p> Recap of to-dos and action items (5 minutes)</p>

   <p> Outcomes:</p>
   <p> Team alignment on content priorities for Q4 2019 and Q1 2020</p>
   <p> Ideas from other departments vetted and refined for inclusion</p>
   <p> Content director able to finish building content strategy for review by team</p>

<p>{!! $data['message'] !!}</p>
