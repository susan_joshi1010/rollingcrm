<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Country;
use App\Project;
use App\IndividualContact;
use App\BusinessContact;
use App\ContactsStatus;
use App\Medium;
use App\InterestLevel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use DB;
use Yajra\Datatables\Datatables;

class ProjectController extends Controller
{
   public function index()
   {
      $contact_status = ContactsStatus::pluck('title', 'id');
      return view('contentmanagement.project.index', compact(['contact_status']));
   }

   public function getProjects()
    {
       $project = Project::orderBy('created_at', 'DESC')->get();
       return Datatables::of($project)
         ->addColumn('actions', function ($project)
            {
               $buttons='<button type="button" project-id="'.$project->id.'" class="view btn btn-primary" title="View"><i class="fa fa-eye" aria-hidden="true"></i> View</button>';
               return $buttons;
            })
      ->rawColumns(['actions'])
      ->make(true);
   }
      
     public function show($project_id)
      {
         $countries = Country::pluck('name', 'id');
         $project = Project::where('id', $project_id)->with('image')->first();
         $contact_status = ContactsStatus::pluck('title', 'id');
         $medium = Medium::pluck('medium', 'id');
         $interest_level = InterestLevel::pluck('interest_level', 'id');
         $compact = ['countries', 'contact_status', 'project_id', 'project', 'medium', 'interest_level'];
         return view('contentmanagement.contact.index', compact($compact));
      }
      
   public function create($contact_type)
   {
      if ($contact_type != 'business' && $contact_type != 'individual') {
         return back();
      }
      $countries = Country::pluck('name', 'id');
      $contact_status = ContactsStatus::pluck('title', 'id');
      $project_id = 0;
      $compact = ['countries', 'contact_status', 'contact_type', 'project_id'];
      return view('contentmanagement.project.create', compact($compact));
   }

   public function showContact($project_id, $contact_id, $contact_type)
   {
      $countries = Country::pluck('name', 'id');
      $contact_status = ContactsStatus::pluck('title', 'id');
      $contact = [];
      if ($contact_id != 0) {
         if ($contact_type == 'individual') {
            $contact = IndividualContact::where("id", $contact_id)->with("image")->first();
         } else if ($contact_type == 'business') {
            $contact = BusinessContact::where("id", $contact_id)->with("image")->first();
         }
      }
      $compact = ['countries', 'contact_status', 'contact_type', 'contact', 'project_id','contact_id'];
      return view('contentmanagement.project.update', compact($compact));
   }

   public function addContactToExistingProject($project_id, $contact_type)
   {
      $countries = Country::pluck('name', 'id');
      $contact_status = ContactsStatus::pluck('title', 'id');
      $compact = ['countries', 'contact_status', 'contact_type', 'project_id'];
      return view('contentmanagement.project.create-add-contact', compact($compact));
   }

   public function update(Request $request, $project_id)
   {
      $this->validate($request, [
         'project_name' => 'required||max:255',
         'objective' => 'required',
         'project_coordinator' => 'required||max:255',
      ]);
      $project = Project::find($project_id);
      
      $project_update = $project->update([
         'project_name' => $request->project_name,
         'project_coordinator' => $request->project_coordinator,
         'objective' => $request->objective,
      ]);

      $documents = $request->file('document');
      if ($documents) {
         foreach ($documents as $key => $document) {
            $extension = $document->getClientOriginalExtension();
            $documentname = $key.time().'.'.$extension;
            $url = 'documents/projects/'.$project_id.'/'.$documentname;
            $original_filename = $document->getClientOriginalName();
            Storage::disk('public')->put($url,  File::get($document));
            $project->image()
            ->create(
                  [   
                     "url" => $url,
                     'original_filename' => $original_filename
                  ]
            );
         }
      }
      return back();
   }
}
