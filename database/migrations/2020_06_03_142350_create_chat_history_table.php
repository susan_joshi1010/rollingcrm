<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChatHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chat_history', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('chatable_id');
            $table->string('chatable_type');
            $table->string('contact_person');
            $table->integer('medium_id')->unsigned();
            $table->foreign('medium_id')->references('id')->on('medium');
            $table->integer('interest_level_id')->unsigned();
            $table->foreign('interest_level_id')->references('id')->on('interest_level');
            $table->longText('description');
            $table->date('next_follow_up');
            $table->integer('entered_by')->unsigned();
            $table->foreign('entered_by')->references('id')->on('user');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chat_history');
    }
}
