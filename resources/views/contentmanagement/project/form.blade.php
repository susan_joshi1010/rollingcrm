<div class="row">
    @if($project_id == 0)
        <div class="form-group col-md-6">
            <label for="project_name">Project Name:</label>
            {!! Form::text('project_name', null, ['class' => 'form-control', 'placeholder' => 'Project Name']) !!}
        </div>
        <div class="form-group col-md-6">
            <label for="project_coordinator">Project Coordinator:</label>
            {!! Form::text('project_coordinator', null, ['class' => 'form-control', 'placeholder' => 'Project Coordinator']) !!}
        </div>
        <div class="form-group col-md-12">
            <label for="objective">Objective:</label>
            {!! Form::textarea('objective', null, ['class' => 'form-control', 'rows' => '2']) !!}
        </div>
        <div class="form-group col-md-12">
            <label for="document">Documents</label>
            <input type="file" id="document" name="document[]" class="form-control-file" onchange="javascript:documentlist()" multiple>
            <div id="documentlist"></div>
        </div>
    @endif
    @if($contact_type == 'individual')
        @include('contentmanagement.contact.individual-contact-form')
    @elseif ($contact_type == 'business')
        @include('contentmanagement.contact.business-contact-form')
    @endif
</div>

