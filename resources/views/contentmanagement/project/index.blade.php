@extends('layout.navbar')

@section('contents')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Projects</h3>
                        <div class="btn-group pull-right">
                            <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Create Project <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li>
                                    <a class="dropdown-item" href="{{route('create-project', 'individual')}}">Individual Contact</a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="{{route('create-project', 'business')}}">Business Contact</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="box-body">
                        <table class="table" id="projects_table">
                            <thead>
                                <tr>
                                    <th>Project Name</th>
                                    <th>Project Coordinator</th>
                                    <th>Created Date</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script>
        $(document).ready( function () {
            let Otable = $("#projects_table");
            Otable.DataTable().destroy();  
            let table = Otable.DataTable({
                "processing": true,
                "serverSide": false,
                "pageLength": 50,
                ajax: {
                    "url" : '/api/content-management/projects',
                },
                columns: [
                    { data: 'project_name' },
                    { data: 'project_coordinator' },
                    { data: 'created_at' },
                    { data: 'actions' },
                ],
                order: [ [2, 'desc'] ], 
                "columnDefs": [
                    {
                        "searchable": false,
                        "targets": [3]
                    }, 
                    {
                        "orderable": false,
                        "targets": [3]
                    },
                ],
                select:true,
                dom: 'Bfrtip',
            });
            table.on('draw.dt', function(){
                $(".view").on('click',function(){
                    window.location.href = '/content-management/projects/create/'+$(this).attr('project-id');
                });
            });
        });
    </script>
@endsection