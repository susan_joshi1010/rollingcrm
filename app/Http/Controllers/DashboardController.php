<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\IndividualContact;
use App\BusinessContact;
use DB;
use Carbon\Carbon;
use \stdClass;

class DashboardController extends Controller
{
    public function home()
    {
        return view('dashboard');
    }

    public function generateContactsReport()
    {
        $ind_contacts = IndividualContact::select('id', 'created_at')
        ->get()
        ->groupBy(function($date) {
            return Carbon::parse($date->created_at)->format('M');
        });
        
        $bus_contacts = BusinessContact::select('id', 'created_at')
        ->get()
        ->groupBy(function($date) {
            return Carbon::parse($date->created_at)->format('M');
        });

        $contacts = [];
        foreach ($ind_contacts as $i_key => $i_val) {
            foreach ($bus_contacts as $b_key => $b_val) {
                $obj = new stdClass();
                $obj->y = $i_key;
                
                if ($i_key == $b_key) {
                    $obj->a = count($i_val);
                    $obj->b = count($b_val);
                } else {
                    if (isset($i_key)) {
                        $obj->a = count($i_val);
                        $obj->b = 0;
                    }
                    if (isset($b_key)) {
                        $obj->a = 0;
                        $obj->b = count($b_val);
                    }
                }
                $contacts[] = $obj;
            } 
        }

        return response()->json(['data' => $contacts], 200);
       
    }
}
