<div class="form-group col-md-4">
    <h5><strong>Organization Name:</strong></h5>
    <div class="contents">{{$contact->organization_name}}</div>
</div>
<div class="form-group col-md-4">
    <h5><strong>Phone Number:</strong></h5>
    <div class="contents">{{$contact->phone_number}}</div>
</div>
<div class="form-group col-md-4">
    <h5><strong>Point of Contact:</strong></h5>
    <div class="contents">{{$contact->point_of_contact}}</div>
</div>
<div class="form-group col-md-4">
    <h5><strong>Point of Contact Number:</strong></h5>
    <div class="contents">{{$contact->point_of_contact_no}}</div>
</div>
<div class="form-group col-md-4">
    <h5><strong>Email:</strong></h5>
    <div class="contents">{{$contact->email}}</div>
</div>
<div class="form-group col-md-4">
    <h5><strong>Address:</strong></h5>
    <div class="contents">{{$contact->address}}</div>
</div>
<div class="form-group col-md-4">
    <h5><strong>Country:</strong></h5>
    <div class="contents">{{$contact->country->name}}</div>
</div>
<div class="form-group col-md-4">
    <h5><strong>Documents:</strong></h5>
    <input type="file" id="document" name="document[]" class="form-control-file" multiple>
</div>
<div class="form-group col-md-4">
    <h5><strong>Status:</strong></h5>
    <div class="contents">{{$contact->status->title}}</div>
</div>
<div class="form-group col-md-12">
    <h5><strong>Remarks:</strong></h5>
    <div class="contents">{{$contact->remarks}}</div>
</div>