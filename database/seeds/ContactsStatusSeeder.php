<?php

use Illuminate\Database\Seeder;

class ContactsStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $status = [
            ["title" => "Qualified Lead", "order" => 1],
            ["title" => "Contact Initiated", "order" => 2],
            ["title" => "Needs Identified", "order" => 3],
            ["title" => "In Negotiation", "order" => 4],
            ["title" => "Deal and Customer", "order" => 5]
        ];

        DB::table('contact_status')->insert($status);
    }
}
