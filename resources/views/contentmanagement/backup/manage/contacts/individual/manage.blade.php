@extends('layout.navbar')

@section('contents')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Manage Existing Individual Contacts</h3>
                        <hr>
                        <div class="form-inline">
                            @foreach ($contact_status as $id => $status)
                                <div class="form-group">
                                    <button class="btn btn-block btn-primary btn-sm getStatusWiseContact" status-id="{{$id}}">{{$status}}</button>
                                </div>
                            @endforeach
                            <div class="form-group">
                                <button class="btn btn-block btn-primary btn-sm getStatusWiseContact" status-id="all-status">All Status</button>
                            </div>
                            <ul class="nav navbar-nav navbar-right">
                                <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Change Status <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    @foreach ($contact_status as $key => $contact_sts)
                                        <li><a href="#" class="change-status" status_id="{{ $key }}">{{ $contact_sts }}</a></li>
                                    @endforeach
                                </ul>
                                </li>
                            </ul>
                        </div>
                        <hr>
                        <form id="search">
                            <div class="form-inline">
                                <div class="form-group">
                                    <label for="name" class="sr-only">Name</label>
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Name">
                                </div>
                                <div class="form-group">
                                    <label for="email" class="sr-only">Email</label>
                                    <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                                </div>
                                <div class="form-group">
                                    <label for="address" class="sr-only">Address</label>
                                    <input type="text" class="form-control" id="address" name="address" placeholder="Address">
                                </div>
                                <div class="form-group">
                                    <label for="phone_number" class="sr-only">Phone Number</label>
                                    <input type="text" class="form-control" id="phone_number" name="phone_number" placeholder="Phone Number">
                                </div>
                            </div>
                            <div class="form-inline" style="padding-top:15px">
                                <div class="form-group">
                                    <label for="joined_from">Joined Date:</label>
                                    <input type="date" class="form-control" id="joined_from" name="joined_from" placeholder="joined_from">
                                </div> &nbsp-&nbsp
                                <div class="form-group">
                                    <input type="date" class="form-control" name="joined_till" placeholder="joined_till">
                                </div>
                                <button type="submit" class="btn btn-primary mb-2 btn-search">Search</button>
                            </div>
                        </form>
                    </div>
                    <div class="box-body">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if (\Session::has('success'))
                            <div class="alert alert-success">
                                <ul>
                                    <li>{!! \Session::get('success') !!}</li>
                                </ul>
                            </div>
                        @endif
                        {!! Form::open(['route' => 'post-create-new-individual-contact', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
                            @include('contentmanagement.project.manage.contacts.individual.form')
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script>
        $(document).ready( function () {
            let Otable = $("#table");
            Otable.DataTable().destroy();  
            let table = Otable.DataTable({
                "processing": true,
                "serverSide": false,
                "pageLength": 50,
                ajax: '/api/content-management/project/manage-old/contacts/individual/manage-old',
                columns: [
                    { data: 'checkbox' },
                    { data: 'created_at' },
                    { data: 'name' },
                    { data: 'status.title' },
                    { data: 'address' },
                    { data: 'phone_number' },
                    { data: 'email' },
                    { data: 'country.name' },
                    { data: 'actions' },
                ], 
                "columnDefs": [
                    {
                        "searchable": false,
                        "targets": [0,6]
                    }, 
                    {
                        "orderable": false,
                        "targets": [0,6]
                    },
                ],
                select:true,
                dom: 'Bfrtip',
            });
            table.on('draw.dt', function(){
                $(".update").on('click',function(){
                    window.location.href = '/content-management/project/create-new/contacts/individual/create-new/'+$(this).attr('contact-id');
                });
                $(".select_all").click(function() {
                    if($(this). prop("checked") == true){
                        $('.select').prop('checked', true);
                    }
                    if($(this). prop("checked") == false){
                        $('.select').prop('checked', false);
                    }
                });
            });

            $(".change-status").on('click', function(){
                let status_id = $(this).attr('status_id');
                var contacts = [];
                $.each($("input[class='select']:checked"), function(){
                    contacts.push($(this).attr('contact-id'));
                });
                if (contacts.length>0) {
                    axios.post('/api/contacts/update-status', {
                        status_id,
                        contacts
                    })
                    .then(function (response) {
                        console.log(response);
                        var table = $('#table').DataTable();
                        table.ajax.reload();
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
                } else {
                    alert("Please choose the contacts first to update them!");
                }
            });

            $('.getStatusWiseContact').on('click', function(){
                let status_id = $(this).attr('status-id');
                let Otable = $("#table");
                Otable.DataTable().destroy();  
                let table = Otable.DataTable({
                    "processing": true,
                    "serverSide": false,
                    "pageLength": 50,
                    ajax: '/api/contacts/individual/filter/status/'+status_id,
                    columns: [
                        { data: 'checkbox' },
                        { data: 'created_at' },
                        { data: 'name' },
                        { data: 'status.title' },
                        { data: 'address' },
                        { data: 'phone_number' },
                        { data: 'email' },
                        { data: 'country.name' },
                        { data: 'actions' },
                    ], 
                    "columnDefs": [
                        {
                            "searchable": false,
                            "targets": [0,6]
                        }, 
                        {
                            "orderable": false,
                            "targets": [0,6]
                        },
                    ],
                    select:true,
                    dom: 'Bfrtip',
                });
                table.on('draw.dt', function(){
                    $(".update").on('click',function(){
                        window.location.href = '/content-management/project/create-new/contacts/individual/create-new/'+$(this).attr('contact-id');
                    });
                    
                    $(".select_all").click(function() {
                        if($(this). prop("checked") == true){
                            $('.select').prop('checked', true);
                        }
                        if($(this). prop("checked") == false){
                            $('.select').prop('checked', false);
                        }
                    });
                });
            });

            $(".btn-search").click(function(e){
                e.preventDefault();
                let data = {
                    name : $("input[name=name]").val(),
                    email : $("input[name=email]").val(),
                    address : $("input[name=address]").val(),
                    phone_number : $("input[name=phone_number]").val(),
                    joined_from : $("input[name=joined_from]").val(),
                    joined_till : $("input[name=joined_till]").val()
                };
                console.log(data);
                let Otable = $("#table");
                Otable.DataTable().destroy();  
                let table = Otable.DataTable({
                    "processing": true,
                    "serverSide": false,
                    "pageLength": 50,
                    "ajax": {
                        'url' : '/api/contacts/individual/filter',
                        "type": "POST",
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    columns: [
                        { data: 'checkbox' },
                        { data: 'created_at' },
                        { data: 'name' },
                        { data: 'status.title' },
                        { data: 'address' },
                        { data: 'phone_number' },
                        { data: 'email' },
                        { data: 'country.name' },
                        { data: 'actions' },
                    ], 
                    "columnDefs": [
                        {
                            "searchable": false,
                            "targets": [0,6]
                        }, 
                        {
                            "orderable": false,
                            "targets": [0,6]
                        },
                    ],
                    select:true,
                    dom: 'Bfrtip',
                });
                table.on('draw.dt', function(){
                    $(".update").on('click',function(){
                        window.location.href = '/content-management/project/create-new/contacts/individual/create-new/'+$(this).attr('contact-id');
                    });
                    
                    $(".select_all").click(function() {
                        if($(this). prop("checked") == true){
                            $('.select').prop('checked', true);
                        }
                        if($(this). prop("checked") == false){
                            $('.select').prop('checked', false);
                        }
                    });
                });
                // axios.post('/api/test', data)
                // .then(function (response) {
                //     console.log(response);
                //     var table = $('#table').DataTable();
                //     table.ajax.reload();
                // })
                // .catch(function (error) {
                //     console.log(error);
                // });
            });

        } );
     </script>
@endsection