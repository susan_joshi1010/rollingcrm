@extends('layout.navbar')

@section('styles')
    <style>
        .wysiwyg-dim {
            overflow : auto;
            width : 100%;
            height : 35vh
        }
        .hide {
            display : none;
        }
        .pad-top-15 {
            padding-top:15px
        }
        .filename{
            margin:10px 0px 10px 0px;
        }
        #file-upload {
            position: absolute;
            right: -9999px;
            visibility: hidden;
            opacity: 0;
        }

        #file-upload-label {
            position: relative;
            padding: 0rem 1rem;
            background: #eee;
            display: inline-block;
            text-align: center;
            overflow: hidden;
            border-radius: 10px;
        }
        #file-upload-label:hover {
            background: #0c8fda;
            color: #fff;
            cursor: pointer;
            transition: 0.2s all;
        }
    </style>
@endsection

@section('contents')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Project | Contacts</h3>
                        <div class="btn-group pull-right">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="caret"></span> Actions
                                </button>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a class="dropdown-item verify-contact" href="#" accept_status='1'><span class="fa fa-key"></span> Activate</a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item verify-contact" href="#" accept_status='2'><span class="fa fa-ban"></span> Reject</a>
                                    </li>
                                </ul>
                            </div>
                            <a href="{{route('marketting-projects')}}" class="btn btn-sm btn-primary">
                               <i class="fa fa-arrow-left" aria-hidden="true"></i> Back 
                            </a>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#project_tab" id="project" class="clicked_tab" data-toggle="tab">Project Details</a></li>
                                <li><a href="#individual_tab" id="individual" class="clicked_tab" data-toggle="tab">Individual Contact</a></li>
                                <li><a href="#business_tab" id="business" class="clicked_tab" data-toggle="tab">Business Contact</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="project_tab">
                                    {!! Form::model($project, ['route' => ['update-project-details', $project->id], 'method' => 'PUT', 'enctype' => 'multipart/form-data', 'class' => 'row', 'id' => "project-form"]) !!}
                                        <div class="form-group col-md-5">
                                            <h3><strong>Project Name:</strong></h3>
                                            <div class="contents" disabled>{{$project->project_name}}</div>
                                        </div>
                                        <div class="form-group col-md-7">
                                            <h3><strong>Project Coordinator:</strong></h3>
                                            <div class="contents">{{$project->project_coordinator}}</div>
                                        </div>
                                        <div class="form-group col-md-12">
                                            <h3><strong>Project url:</strong></h3>
                                            <div class="contents">
                                                <b>Individual Project: </b><a target="_blank" href="{{URL::to('/')}}/business-developement/projects/{{$project->slug}}/create/contact/individual">{{URL::to('/')}}/business-developement/projects/{{$project->slug}}/create/contact/individual</a>
                                            </div>
                                            <div class="contents">
                                                <b>Business Project: </b><a target="_blank" href="{{URL::to('/')}}/business-developement/projects/{{$project->slug}}/create/contact/business">{{URL::to('/')}}/business-developement/projects/{{$project->slug}}/create/contact/business</a>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-12">
                                            <h3><strong>Objective:</strong></h3>
                                            <div class="wysiwyg-dim">
                                                <div class="contents">{!! $project->objective !!}</div>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-12">
                                            <h3><strong>Documents:</strong></h3>
                                            <div id="documentlist">
                                                <ul>
                                                    @foreach($project->image as $img)
                                                        <li class="filename">{{$img->original_filename}}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    {!! Form::close() !!}
                                </div>
                                <div class="tab-pane" id="individual_tab">
                                    <div class="form-inline col-md-10">
                                        @foreach ($contact_status as $id => $status)
                                            <div class="form-group">
                                                <button class="btn btn-block btn-success btn-sm getStatusWiseIndContact" status-id="{{$id}}">{{$status}}</button>
                                            </div>
                                        @endforeach
                                        <div class="form-group">
                                            <button class="btn btn-block btn-success btn-sm getStatusWiseIndContact" status-id="all-status">All Status</button>
                                        </div>
                                    </div>
                                    <div class="col-md-2 form-group">
                                        <button class="btn btn-block btn-danger btn-sm resetindiviual"><i class="fa fa-undo" aria-hidden="true"></i> Reset</button>
                                    </div>
                                    <form id="search" class="pad-top-15">
                                        <div class="form-inline">
                                            <div class="form-group">
                                                <label for="name" class="sr-only">Name</label>
                                                <input type="text" class="form-control individualtable" id="name" name="name" placeholder="Name">
                                            </div>
                                            <div class="form-group">
                                                <label for="email" class="sr-only">Email</label>
                                                <input type="email" class="form-control individualtable" id="email" name="email" placeholder="Email">
                                            </div>
                                            <div class="form-group">
                                                <label for="address" class="sr-only">Address</label>
                                                <input type="text" class="form-control individualtable" id="address" name="address" placeholder="Address">
                                            </div>
                                            <div class="form-group">
                                                <label for="phone_number" class="sr-only">Phone Number</label>
                                                <input type="text" class="form-control individualtable" id="phone_number" name="phone_number" placeholder="Phone Number">
                                            </div>
                                            <!-- <div class="form-group">
                                                <label for="accept_status" class="sr-only">Status</label>
                                                {!! Form::select('accept_status', [4 => "All", 0 => "Pending", 1 => "Accepted", 2 => "Rejected"], null, ['class' => 'form-control individualtable']) !!}
                                            </div> -->
                                        </div>
                                        <div class="form-inline pad-top-15">
                                            <div class="form-group">
                                                <label for="joined_from">Joined Date:</label>
                                                <input type="date" class="form-control individualtable" id="joined_from" name="joined_from" placeholder="joined_from">
                                            </div> &nbsp-&nbsp
                                            <div class="form-group">
                                                <input type="date" class="form-control individualtable" name="joined_till" placeholder="joined_till">
                                            </div>
                                            <button type="submit" id="ind-filter-search" class="btn btn-primary mb-2 btn-search">Search</button>
                                        </div>
                                    </form>
                                    <table class="table" id="individual_table">
                                        <thead>
                                        <tr>
                                            <th><input type="checkbox" name="select_all" id="ind_table_select_all" value="select_all"><br></th>
                                            <th>Joined Date</th>
                                            <th>Name</th>
                                            <th>Status</th>
                                            <th>Contact Status</th>
                                            <th>Address</th>
                                            <th>Contact</th>
                                            <th>Email</th>
                                            <th>Country</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                                <div class="tab-pane" id="business_tab">
                                    <div class="form-inline col-md-10">
                                        @foreach ($contact_status as $id => $status)
                                            <div class="form-group">
                                                <button class="btn btn-block btn-success btn-sm getStatusWiseBusContact" status-id="{{$id}}">{{$status}}</button>
                                            </div>
                                        @endforeach
                                        <div class="form-group">
                                            <button class="btn btn-block btn-success btn-sm getStatusWiseBusContact" status-id="all-status">All Status</button>
                                        </div>
                                    </div>
                                    <div class="col-md-2 form-group">
                                        <button class="btn btn-block btn-danger btn-sm resetbusiness"><i class="fa fa-undo" aria-hidden="true"></i> Reset</button>
                                    </div>
                                    <form id="search" class="pad-top-15">
                                        <div class="form-inline">
                                            <div class="form-group">
                                                <label for="name" class="sr-only">Organization Name</label>
                                                <input type="text" class="form-control businesstable" id="organization_name" name="organization_name" placeholder="Organization Name">
                                            </div>
                                            <div class="form-group">
                                                <label for="name" class="sr-only">Point Of Contact</label>
                                                <input type="text" class="form-control businesstable" id="point_of_contact" name="point_of_contact" placeholder="Point Of Contact">
                                            </div>
                                            <div class="form-group">
                                                <label for="email" class="sr-only">Email</label>
                                                <input type="email" class="form-control businesstable" id="email" name="email" placeholder="Email">
                                            </div>
                                            <div class="form-group">
                                                <label for="address" class="sr-only">Address</label>
                                                <input type="text" class="form-control businesstable" id="address" name="address" placeholder="Address">
                                            </div>
                                            <div class="form-group">
                                                <label for="phone_number" class="sr-only">Phone Number</label>
                                                <input type="text" class="form-control businesstable" id="phone_number" name="phone_number" placeholder="Phone Number">
                                            </div>
                                            <!-- <div class="form-group">
                                                <label for="accept_status" class="sr-only">Status</label>
                                                {!! Form::select('accept_status', [null => "All", 0 => "Pending", 1 => "Accepted", 2 => "Rejected"], null, ['class' => 'form-control businesstable']) !!}
                                            </div> -->
                                        </div>
                                        <div class="form-inline pad-top-15">
                                            <div class="form-group">
                                                <label for="joined_from">Joined Date:</label>
                                                <input type="date" class="form-control businesstable" id="joined_from" name="joined_from" placeholder="joined_from">
                                            </div> &nbsp-&nbsp
                                            <div class="form-group">
                                                <input type="date" class="form-control businesstable" id="joined_till" name="joined_till" placeholder="joined_till">
                                            </div>
                                            <button type="submit" id="buss-filter-search" class="btn btn-primary mb-2 btn-buss-search">Search</button>
                                        </div>
                                    </form>
                                    <table class="table" id="business_table">
                                        <thead>
                                        <tr>
                                            <th><input type="checkbox" name="select_all" id="bus_table_select_all" value="select_all"><br></th>
                                            <th>Joined Date</th>
                                            <th>Name</th>
                                            <th>Status</th>
                                            <th>Contact Status</th>
                                            <th>P.O.C.</th>
                                            <th>P.O.C. No.</th>
                                            <th>Address</th>
                                            <th>Contact</th>
                                            <th>Email</th>
                                            <th>Country</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script>
        $(document).ready( function () {
            //if any tab is clicked restore results from localstorage
            if (localStorage.getItem("clicked_tab") != null && localStorage.getItem("clicked_tab") != project) {
                setTimeout(function(){
                    $("#"+localStorage.getItem("clicked_tab")).click()
                });
                if (localStorage.getItem("individual_filter_name") != null) {
                    setTimeout(function(){
                        $('.individualtable[id=name]').val(localStorage.getItem("individual_filter_name"));
                    });
                }
                if (localStorage.getItem("individual_filter_email") != null) {
                    setTimeout(function(){
                        $('.individualtable[id=email]').val(localStorage.getItem("individual_filter_email"));
                    });
                }
                if (localStorage.getItem("individual_filter_address") != null) {
                    setTimeout(function(){
                        $('.individualtable[id=address]').val(localStorage.getItem("individual_filter_address"));
                    });
                }
                if (localStorage.getItem("individual_filter_phone_number") != null) {
                    setTimeout(function(){
                        $('.individualtable[id=phone_number]').val(localStorage.getItem("individual_filter_phone_number"));
                    });
                }
                if (localStorage.getItem("individual_filter_joined_from") != null) {
                    setTimeout(function(){
                        $('.individualtable[id=joined_from]').val(localStorage.getItem("individual_filter_joined_from"));
                    });
                }
                if (localStorage.getItem("individual_filter_joined_till") != null) {
                    setTimeout(function(){
                        $('.individualtable[id=joined_till]').val(localStorage.getItem("individual_filter_joined_till"));
                    });
                }
                if (
                    localStorage.getItem("status_id") != null || 
                    localStorage.getItem("individual_filter_name") != null ||
                    localStorage.getItem("individual_filter_email") != null ||
                    localStorage.getItem("individual_filter_address") != null ||
                    localStorage.getItem("individual_filter_phone_number") != null ||
                    localStorage.getItem("individual_filter_joined_from") != null ||
                    localStorage.getItem("individual_filter_joined_till") != null                    
                    ) {
                        setTimeout(function(){
                            if (localStorage.getItem("status_id") != null) {
                                setTimeout(function(){
                                    $('.getStatusWiseIndContact[status-id='+localStorage.getItem("status_id")+']').removeClass('btn-success').addClass("btn-primary");
                                });
                            }
                            $("#ind-filter-search").click();
                        });
                }else{
                    if (localStorage.getItem("status_id") != null) {
                        setTimeout(function(){
                            $('.getStatusWiseIndContact[status-id='+localStorage.getItem("status_id")+']').click()
                        });
                    }
                }

                if (localStorage.getItem("business_filter_organization_name") != null) {
                    setTimeout(function(){
                        $('.businesstable[id=organization_name]').val(localStorage.getItem("business_filter_organization_name"));
                    });
                }
                if (localStorage.getItem("business_filter_point_of_contact") != null) {
                    setTimeout(function(){
                        $('.businesstable[id=point_of_contact]').val(localStorage.getItem("business_filter_point_of_contact"));
                    });
                }
                if (localStorage.getItem("business_filter_email") != null) {
                    setTimeout(function(){
                        $('.businesstable[id=email]').val(localStorage.getItem("business_filter_email"));
                    });
                }
                if (localStorage.getItem("business_filter_address") != null) {
                    setTimeout(function(){
                        $('.businesstable[id=address]').val(localStorage.getItem("business_filter_address"));
                    });
                }
                if (localStorage.getItem("business_filter_phone_number") != null) {
                    setTimeout(function(){
                        $('.businesstable[id=phone_number]').val(localStorage.getItem("business_filter_phone_number"));
                    });
                }
                if (localStorage.getItem("business_filter_joined_from") != null) {
                    setTimeout(function(){
                        $('.businesstable[id=joined_from]').val(localStorage.getItem("business_filter_joined_from"));
                    });
                }
                if (localStorage.getItem("business_filter_joined_till") != null) {
                    setTimeout(function(){
                        $('.businesstable[id=joined_till]').val(localStorage.getItem("business_filter_joined_till"));
                    });
                }
                if (
                    localStorage.getItem("bus_status_id") != null || 
                    localStorage.getItem("business_filter_organization_name") != null ||
                    localStorage.getItem("business_filter_point_of_contact") != null ||
                    localStorage.getItem("business_filter_email") != null ||
                    localStorage.getItem("business_filter_address") != null ||
                    localStorage.getItem("business_filter_phone_number") != null ||
                    localStorage.getItem("business_filter_joined_from") != null ||
                    localStorage.getItem("business_filter_joined_till") != null                    
                    ) {
                        setTimeout(function(){
                            if (localStorage.getItem("bus_status_id") != null) {
                                setTimeout(function(){
                                    $('.getStatusWiseBusContact[status-id='+localStorage.getItem("bus_status_id")+']').removeClass('btn-success').addClass("btn-primary");
                                });
                            }
                            $("#buss-filter-search").click();
                        });
                }else{
                    if (localStorage.getItem("bus_status_id") != null) {
                        setTimeout(function(){
                            $('.getStatusWiseBusContact[status-id='+localStorage.getItem("bus_status_id")+']').click()
                        });
                    }
                }
            }
            //if any tab is clicked restore results from localstorage

            // save clicked tab status to local storage
            $(".clicked_tab").on("click",function(){
                localStorage.setItem("clicked_tab", $(this).attr('id'));
            });
            //save clicked tab status to local storage

            //save individual inputs to local storage
            $(".individualtable").on("keyup change",function(){
                localStorage.setItem("individual_filter_name", $("input[name=name].individualtable").val());
                localStorage.setItem("individual_filter_email", $("input[name=email].individualtable").val());
                localStorage.setItem("individual_filter_address", $("input[name=address].individualtable").val());
                localStorage.setItem("individual_filter_phone_number", $("input[name=phone_number].individualtable").val());
                localStorage.setItem("individual_filter_joined_from", $("input[name=joined_from].individualtable").val());
                localStorage.setItem("individual_filter_joined_till", $("input[name=joined_till].individualtable").val());
            });

            $("input[name=name].individualtable").keyup(function(e){
                if( this.value.length < 3 ) return;
                axios.post('/api/business-developement/marketting/project/contacts/individual/filter/autocomplete', {
                    name: this.value,
                    project_id: {{$project_id}},
                })
                .then(function (response) {
                    $("input[name=name].individualtable").autocomplete({
                    source: response.data
                    });
                })
                .catch(function (error) {
                    console.log(error);
                });
            });
            //save business inputs to local storage

            //save business inputs to local storage
            $(".businesstable").on("keyup change",function(){
                localStorage.setItem("business_filter_organization_name", $("input[name=organization_name].businesstable").val());
                localStorage.setItem("business_filter_point_of_contact", $("input[name=point_of_contact].businesstable").val());
                localStorage.setItem("business_filter_email", $("input[name=email].businesstable").val());
                localStorage.setItem("business_filter_address", $("input[name=address].businesstable").val());
                localStorage.setItem("business_filter_phone_number", $("input[name=phone_number].businesstable").val());
                localStorage.setItem("business_filter_joined_from", $("input[name=joined_from].businesstable").val());
                localStorage.setItem("business_filter_joined_till", $("input[name=joined_till].businesstable").val());
            });

            $("input[name=organization_name].businesstable, input[name=point_of_contact].businesstable, input[name=address].businesstable, input[name=email].businesstable, input[name=phone_number].businesstable").keyup(function(e){
                if( this.value.length < 3 ) return;
                var data = {};
                let key = $(this).attr('name');
                let val = $(this).val();
                data[key] = val;
                data['project_id'] = {{$project_id}};
                let that = $(this);
                axios.post('/api/business-developement/marketting/project/contacts/business/filter/autocomplete', data)
                .then(function (response) {
                    setTimeout(function(){
                        that.autocomplete({
                            source: response.data
                        });
                    },50);
                })
                .catch(function (error) {
                    console.log(error);
                });
            });
            //save individual inputs to local storage

            // indiviual table initialization
            function initializeIndividualTable() {
                let Otable = $("#individual_table");
                Otable.DataTable().destroy();  
                let ind_table = Otable.DataTable({
                    "processing": true,
                    "serverSide": false,
                    "pageLength": 50,
                    ajax: {
                        "url" : '/api/business-development/marketting/projects/{{$project_id}}/contacts/individual',
                    },
                    order: [ [1, 'desc'] ], 
                    columns: [
                        { data: 'checkbox' },
                        { data: 'created_at' },
                        { data: 'name' },
                        { data: 'active_status' },
                        { data: 'status.title' },
                        { data: 'address' },
                        { data: 'phone_number' },
                        { data: 'email' },
                        { data: 'country.name' },
                        { data: 'actions' },
                    ],
                    "columnDefs": [
                        {
                            "searchable": false,
                            "targets": [0,8]
                        }, 
                        {
                            "orderable": false,
                            "targets": [0,8]
                        },
                        {
                            "targets": 3,
                            "render": function (data, type, row) {
                                let active_status = '';
                                if (row.accept_status == 0) {
                                    active_status = "<span class='label label-default'>Pending</span>";
                                } else if (row.accept_status == 1) {
                                    active_status = '<span class="label label-success">Accepted</span>';
                                } else {
                                    active_status = '<span class="label label-danger">Rejected</span>';
                                }
                                return active_status;
                            }
                        }
                    ], 
                    select:true,
                    dom: 'Bfrtip',
                });
                ind_table.on('draw.dt', function(){
                    $("#ind_table_select_all").click(function() {
                        if($(this). prop("checked") == true){
                            $('.ind_select').prop('checked', true);
                        }
                        if($(this). prop("checked") == false){
                            $('.ind_select').prop('checked', false);
                        }
                    });

                    $(".view-ind").click(function() {
                        window.location.href = '/business-developement/projects/create/contact/'+$(this).attr('contact_id')+'/individual';
                    });

                    $(".verify-ind-contact").click(function(e) {
                        Swal.fire({
                        title: 'Are you sure?',
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes!'
                        }).then((result) => {
                            if (result.value) {
                                axios.put('/business-developement/projects/contact/'+$(this).attr('contact_id')+'/'+$(this).attr('contact_type')+'/accept/'+$(this).attr('accept_status'))
                                .then(function (response) {
                                    location.reload();
                                })
                                .catch(function (error) {
                                    console.log(error);
                                });
                            }
                        });
                    });

                    $(".verify-contact").on("click", function(){
                        var contact_id = [];
                        let contact_type = $("ul li.active > a").attr('id');
                        let class_name = '';
                        if (contact_type == 'individual') {
                            class_name = 'ind_select';
                        } else{
                            class_name = 'bus_select';
                        }
                        $.each($("input[class="+class_name+"]:checked"), function(){
                            contact_id.push($(this).attr('contact-id'));
                        });
                        if (contact_id.length > 0) {
                            Swal.fire({
                            title: 'Are you sure?',
                            icon: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Yes!'
                            }).then((result) => {
                                if (result.value) {
                                    axios.post('/api/business-developement/projects/contact/'+contact_type+'/accept/'+$(this).attr('accept_status'),
                                    {
                                        contact_id
                                    })
                                    .then(function (response) {
                                        location.reload();
                                    })
                                    .catch(function (error) {
                                        console.log(error);
                                    });
                                }
                            });
                            
                        } else {
                            alert("Please choose the contacts first to update them!");
                        }
                    });
                });
            }
            // indiviual table initialization

            // business table initalization
            function initializeBusinessTable() {
                let Btable = $("#business_table");
                Btable.DataTable().destroy();  
                let bus_table = Btable.DataTable({
                    "processing": true,
                    "serverSide": false,
                    "pageLength": 50,
                    order: [ [1, 'desc'] ], 
                    ajax: '/api/business-development/marketting/projects/{{$project_id}}/contacts/business',
                    columns: [
                        { data: 'checkbox' },
                        { data: 'created_at' },
                        { data: 'organization_name' },
                        { data: 'active_status' },
                        { data: 'status.title' },
                        { data: 'point_of_contact' },
                        { data: 'point_of_contact_no' },
                        { data: 'address' },
                        { data: 'phone_number' },
                        { data: 'email' },
                        { data: 'country.name' },
                        { data: 'actions' },
                    ], 
                    "columnDefs": [
                        {
                            "searchable": false,
                            "targets": [0,10]
                        }, 
                        {
                            "orderable": false,
                            "targets": [0,10]
                        },
                        {
                            "targets": 3,
                            "render": function (data, type, row) {
                                let active_status = '';
                                if (row.accept_status == 0) {
                                    active_status = "<span class='label label-default'>Pending</span>";
                                } else if (row.accept_status == 1) {
                                    active_status = '<span class="label label-success">Accepted</span>';
                                } else {
                                    active_status = '<span class="label label-danger">Rejected</span>';
                                }
                                return active_status;
                            }
                        }
                    ],
                    select:true,
                    dom: 'Bfrtip',
                });
                bus_table.on('draw.dt', function(){
                    $("#bus_table_select_all").click(function() {
                        if($(this). prop("checked") == true){
                            $('.bus_select').prop('checked', true);
                        }
                        if($(this). prop("checked") == false){
                            $('.bus_select').prop('checked', false);
                        }
                    });
                    $(".view-bus").click(function() {
                        window.location.href = '/business-developement/projects/create/contact/'+$(this).attr('contact_id')+'/business';
                    });
                    $(".verify-bus-contact").click(function() {
                        Swal.fire({
                        title: 'Are you sure?',
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes!'
                        }).then((result) => {
                            if (result.value) {
                                axios.put('/business-developement/projects/contact/'+$(this).attr('contact_id')+'/'+$(this).attr('contact_type')+'/accept/'+$(this).attr('accept_status'))
                                .then(function (response) {
                                    location.reload();
                                })
                                .catch(function (error) {
                                    console.log(error);
                                });
                            }
                        });
                    });
                });
            }
            // business table initalization

            initializeIndividualTable();
            initializeBusinessTable();

            function resetIndividualFilterData() {
                localStorage.clear();
                $(".getStatusWiseIndContact").removeClass('btn-primary').addClass('btn-success');
                $("input[name=name].individualtable").val("");
                $("input[name=email].individualtable").val("");
                $("input[name=address].individualtable").val("");
                $("input[name=phone_number].individualtable").val("");
                $("input[name=joined_from].individualtable").val("");
                $("input[name=joined_till].individualtable").val("");
                initializeIndividualTable();
            }

            $(".resetindiviual").on("click", function(){
                resetIndividualFilterData();
            });

            function resetBusinessFilterData() {
                localStorage.clear();
                $(".getStatusWiseBusContact").removeClass('btn-primary').addClass('btn-success');
                $("input[name=organization_name].businesstable").val("");
                $("input[name=point_of_contact].businesstable").val("");
                $("input[name=email].businesstable").val("");
                $("input[name=address].businesstable").val("");
                $("input[name=phone_number].businesstable").val("");
                $("input[name=joined_from].businesstable").val("");
                $("input[name=joined_till].businesstable").val("");
                initializeBusinessTable();
            }

            $(".resetbusiness").on("click", function(){
                resetBusinessFilterData();
            });

            //filter individual contacts status wise
            $('.getStatusWiseIndContact').on('click', function(e){
                $(".getStatusWiseIndContact").removeClass('btn-primary').addClass('btn-success');
                $(this).removeClass('btn-success').addClass('btn-primary');
                let status_id = $(".getStatusWiseIndContact.btn-primary").attr('status-id');
                localStorage.setItem("status_id", status_id);
                filterIndividualContact(status_id);
            });
            $('.btn-search').on('click', function(e){
                e.preventDefault();
                $(".getStatusWiseIndContact").removeClass('btn-primary').addClass('btn-success');
                $(".getStatusWiseIndContact[status-id="+localStorage.getItem("status_id")+"]").removeClass('btn-success').addClass('btn-primary');
                filterIndividualContact(localStorage.getItem("status_id"));
            });
            function filterIndividualContact(status_id) {
                setTimeout(function(){
                    let Otable = $("#individual_table");
                    Otable.DataTable().destroy();  
                    let ind_table = Otable.DataTable({
                        "processing": true,
                        "serverSide": false,
                        "pageLength": 50,
                        "ajax": {
                            'url' : '/api/business-developement/marketting/project/contacts/individual/filter',
                            "type": "POST",
                            "headers": {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                        "pageLength": 50,
                            "data" : {
                                name : $("input[name=name]").val(),
                                // accept_status : $("input[name=accept_status]").val(),
                                email : $("input[name=email]").val(),
                                address : $("input[name=address]").val(),
                                phone_number : $("input[name=phone_number]").val(),
                                joined_from : $("input[name=joined_from]").val(),
                                joined_till : $("input[name=joined_till]").val(),
                                project_id : {{$project_id}},
                                status_id
                            }
                        },
                        order: [ [1, 'desc'] ], 
                        columns: [
                            { data: 'checkbox' },
                            { data: 'created_at' },
                            { data: 'name' },
                            { data: 'active_status' },
                            { data: 'status.title' },
                            { data: 'address' },
                            { data: 'phone_number' },
                            { data: 'email' },
                            { data: 'country.name' },
                            { data: 'actions' },
                        ],
                        "columnDefs": [
                            {
                                "searchable": false,
                                "targets": [0,8]
                            }, 
                            {
                                "orderable": false,
                                "targets": [0,8]
                            },
                            {
                                "targets": 3,
                                "render": function (data, type, row) {
                                    let active_status = '';
                                    if (row.accept_status == 0) {
                                        active_status = "<span class='label label-default'>Pending</span>";
                                    } else if (row.accept_status == 1) {
                                        active_status = '<span class="label label-success">Accepted</span>';
                                    } else {
                                        active_status = '<span class="label label-danger">Rejected</span>';
                                    }
                                    return active_status;
                                }
                            }
                        ], 
                        select:true,
                        dom: 'Bfrtip',
                    });
                    ind_table.on('draw.dt', function(){
                        $("#ind_table_select_all").click(function() {
                            if($(this). prop("checked") == true){
                                $('.ind_select').prop('checked', true);
                            }
                            if($(this). prop("checked") == false){
                                $('.ind_select').prop('checked', false);
                            }
                        });

                        $(".view-ind").click(function() {
                            window.location.href = '/business-developement/projects/create/contact/'+$(this).attr('contact_id')+'/individual';
                        });

                        $(".verify-ind-contact").click(function(e) {
                            Swal.fire({
                            title: 'Are you sure?',
                            icon: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Yes!'
                            }).then((result) => {
                                if (result.value) {
                                    axios.put('/business-developement/projects/contact/'+$(this).attr('contact_id')+'/'+$(this).attr('contact_type')+'/accept/'+$(this).attr('accept_status'))
                                    .then(function (response) {
                                        location.reload();
                                    })
                                    .catch(function (error) {
                                        console.log(error);
                                    });
                                }
                            });
                        });

                        $(".verify-contact").on("click", function(){
                            var contact_id = [];
                            let contact_type = $("ul li.active > a").attr('id');
                            let class_name = '';
                            if (contact_type == 'individual') {
                                class_name = 'ind_select';
                            } else{
                                class_name = 'bus_select';
                            }
                            $.each($("input[class="+class_name+"]:checked"), function(){
                                contact_id.push($(this).attr('contact-id'));
                            });
                            if (contact_id.length > 0) {
                                Swal.fire({
                                title: 'Are you sure?',
                                icon: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: '#3085d6',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Yes!'
                                }).then((result) => {
                                    if (result.value) {
                                        axios.post('/api/business-developement/projects/contact/'+contact_type+'/accept/'+$(this).attr('accept_status'),
                                        {
                                            contact_id
                                        })
                                        .then(function (response) {
                                            location.reload();
                                        })
                                        .catch(function (error) {
                                            console.log(error);
                                        });
                                    }
                                });
                                
                            } else {
                                alert("Please choose the contacts first to update them!");
                            }
                        });
                    });
                }, 100);
            }
            //filter individual contacts status wise


            //filter business contacts status wise
            $('.getStatusWiseBusContact').on('click', function(e){
                $(".getStatusWiseBusContact").removeClass('btn-primary').addClass('btn-success');
                $(this).removeClass('btn-success').addClass('btn-primary');
                let bus_status_id = $(".getStatusWiseBusContact.btn-primary").attr('status-id');
                localStorage.setItem("bus_status_id", bus_status_id);
                filterBusinessContact(bus_status_id);
            });
            $('.btn-buss-search').on('click', function(e){
                e.preventDefault();
                $(".getStatusWiseBusContact").removeClass('btn-primary').addClass('btn-success');
                $(".getStatusWiseBusContact[status-id="+localStorage.getItem("bus_status_id")+"]").removeClass('btn-success').addClass('btn-primary');
                filterBusinessContact(localStorage.getItem("bus_status_id"));
            });
            function filterBusinessContact(bus_status_id) {
                setTimeout(function(){
                    let Btable = $("#business_table");
                    Btable.DataTable().destroy();  
                    let bus_table = Btable.DataTable({
                        "processing": true,
                        "serverSide": false,
                        "pageLength": 50,
                        order: [ [1, 'desc'] ], 
                        "ajax": {
                            'url' : '/api/business-developement/marketting/project/contacts/business/filter',
                            "type": "POST",
                            "headers": {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            "data" : {
                                organization_name : $(".businesstable[name=organization_name]").val(),
                                point_of_contact : $(".businesstable[name=point_of_contact]").val(),
                                // accept_status : $(".businesstable[name=accept_status]").val(),
                                email : $(".businesstable[name=email]").val(),
                                address : $(".businesstable[name=address]").val(),
                                phone_number : $(".businesstable[name=phone_number]").val(),
                                joined_from : $(".businesstable[name=joined_from]").val(),
                                joined_till : $(".businesstable[name=joined_till]").val(),
                                project_id : {{$project_id}},
                                status_id : bus_status_id
                            }
                        },
                        columns: [
                            { data: 'checkbox' },
                            { data: 'created_at' },
                            { data: 'organization_name' },
                            { data: 'active_status' },
                            { data: 'status.title' },
                            { data: 'point_of_contact' },
                            { data: 'point_of_contact_no' },
                            { data: 'address' },
                            { data: 'phone_number' },
                            { data: 'email' },
                            { data: 'country.name' },
                            { data: 'actions' },
                        ], 
                        "columnDefs": [
                            {
                                "searchable": false,
                                "targets": [0,10]
                            }, 
                            {
                                "orderable": false,
                                "targets": [0,10]
                            },
                            {
                                "targets": 3,
                                "render": function (data, type, row) {
                                    let active_status = '';
                                    if (row.accept_status == 0) {
                                        active_status = "<span class='label label-default'>Pending</span>";
                                    } else if (row.accept_status == 1) {
                                        active_status = '<span class="label label-success">Accepted</span>';
                                    } else {
                                        active_status = '<span class="label label-danger">Rejected</span>';
                                    }
                                    return active_status;
                                }
                            }
                        ],
                        select:true,
                        dom: 'Bfrtip',
                    });
                    bus_table.on('draw.dt', function(){
                        $("#bus_table_select_all").click(function() {
                            if($(this). prop("checked") == true){
                                $('.bus_select').prop('checked', true);
                            }
                            if($(this). prop("checked") == false){
                                $('.bus_select').prop('checked', false);
                            }
                        });
                        $(".view-bus").click(function() {
                            window.location.href = '/business-developement/projects/create/contact/'+$(this).attr('contact_id')+'/business';
                        });
                        $(".verify-bus-contact").click(function() {
                            Swal.fire({
                            title: 'Are you sure?',
                            icon: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Yes!'
                            }).then((result) => {
                                if (result.value) {
                                    axios.put('/business-developement/projects/contact/'+$(this).attr('contact_id')+'/'+$(this).attr('contact_type')+'/accept/'+$(this).attr('accept_status'))
                                    .then(function (response) {
                                        location.reload();
                                    })
                                    .catch(function (error) {
                                        console.log(error);
                                    });
                                }
                            });
                        });
                    });
                }, 100);
            }
            //filter business contacts status wise

            $(".view-ind").on("click", function(){
                window.location.href = '/content-management/projects/'+{{$project->id}}+'/contact/individual/'+$(this).attr('contact-id');
            });
        });
    </script>
@endsection