<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\IndividualContact;
use App\BusinessContact;
use DB;
use App\ContactsStatus;
use Yajra\Datatables\Datatables;

class ContactStatusController extends Controller
{
    public function updateStatus(Request $request, $contact_type) {
        DB::beginTransaction();
        try{
            if ($contact_type == 'individual'){
                foreach ($request->contacts as $contact) {
                    IndividualContact::find($contact)->update([ "status_id" => $request->status_id]);            
                }
            } else if ($contact_type == 'business') {
                foreach ($request->contacts as $contact) {
                    BusinessContact::find($contact)->update([ "status_id" => $request->status_id]);            
                }
            }

            DB::commit();
            return response()->json(["status" => 200, "msg" => "Sucessfully updated."], 200);
            
        }catch(\Exception $errors){
            DB::rollback();
            return response()->json($errors->getMessage(), 500);
        }
    }

    public function getIndividualContactStatusWise($status_id)
    {
        $contacts = IndividualContact::orderBy('created_at', 'DESC')
            ->when($status_id!='all-status', function($query) use ($status_id) {
                $query->where('status_id', $status_id);
            })
            ->with('status', 'country')
            ->get();
        return Datatables::of($contacts)
         ->addColumn('checkbox', function ($contacts)
         {
            $checkbox='<input type="checkbox" class="select" contact-id='.$contacts->id.'><br>';
            return $checkbox;
         })->addColumn('actions', function ($contacts)
         {
            $buttons='<button type="button" id="'.$contacts->id.'" class="update-ind btn btn-primary" title="Update" contact-id='.$contacts->id.'><i class="fa fa-refresh" aria-hidden="true"></i>  Edit</button>';
            return $buttons;
         })
         ->rawColumns(['checkbox', 'actions'])
         ->make(true);
    }
}
