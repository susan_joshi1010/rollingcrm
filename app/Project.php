<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'project';

    public function image()
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    public function individualContact()
    {
        return $this->hasMany('App\IndividualContact');
    }

    public function businessContact()
    {
        return $this->hasMany('App\BusinessContact');
    }
}
