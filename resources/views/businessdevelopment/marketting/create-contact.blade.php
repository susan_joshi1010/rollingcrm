@extends('layout.navbar')

@section('contents')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Project | {{$contact_type}} contact </h3>
                    </div>
                    <div class="box-body">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if (\Session::has('success'))
                            <div class="alert alert-success">
                                <ul>
                                    <li>{!! \Session::get('success') !!}</li>
                                </ul>
                            </div>
                        @endif
                        {!! Form::model($project) !!}
                            <div class="form-group col-md-6">
                                <h3><strong>Project Name:</strong></h3>
                                <div class="contents" disabled>{{$project->project_name}}</div>
                            </div>
                            <div class="form-group col-md-6">
                                <h3><strong>Project Coordinator:</strong></h3>
                                <div class="contents">{{$project->project_coordinator}}</div>
                            </div>
                            <div class="form-group col-md-11">
                                <h3><strong>Objective:</strong></h3>
                                <div class="wysiwyg-dim">
                                    <div class="contents">{!! $project->objective !!}</div>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <h3>
                                    <strong>Document:</strong>
                                </h3>
                                <div id="documentlist">
                                    <ul>
                                        @foreach($project->image as $img)
                                            <li class="filename"><a download href="{{ asset('uploads/'.$img->url)}}">{{$img->original_filename}}</a></li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        {!! Form::close() !!}

                        @if($contact_type == 'individual')
                            {!! Form::open(['route' => ['marketting-store-individual', $slug], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
                        @elseif ($contact_type == 'business')
                            {!! Form::open(['route' => ['marketting-store-business', $slug], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
                        @endif
                        <div class="row">
                            @if($contact_type == 'individual')
                                @include('contentmanagement.contact.individual-contact-form')
                            @elseif ($contact_type == 'business')
                                @include('contentmanagement.contact.business-contact-form')
                            @endif
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary pull-right">Submit</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>   
        </div>
    </section>
@endsection
@section('scripts')
<script>
    $(function () {
        CKEDITOR.replace( 'objective' );
    })
    $('body').addClass('skin-blue sidebar-mini sidebar-collapse').removeClass('theClassThatsTherenow');
</script>
@endsection