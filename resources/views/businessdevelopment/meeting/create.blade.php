@extends('layout.navbar')


@section('contents')
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Create Meeting</h3>
                        <a href="{{url('/business-development/meeting/index')}}" class="btn btn-primary btn-sm pull-right">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back 
                        </a>
                    </div>
                    <div class="box-body">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if (\Session::has('success'))
                            <div class="alert alert-success">
                                <ul>
                                    <li>{!! \Session::get('success') !!}</li>
                                </ul>
                            </div>
                        @endif
                        <!-- <div class="row"> -->
                            {!! Form::open(['route' => 'store-meeting', 'method' => 'POST', 'id' =>'meeting_form' ,'enctype' => 'multipart/form-data']) !!}
                                <div class="form-group col-md-6">
                                    <label for="title">Title:</label>
                                         {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Title']) !!}
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="time">Meeting Time:</label>
                                         {!! Form::time('time', null, ['class' => 'form-control', 'placeholder' => 'Meeting Time']) !!}
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="date">Meeting Date:</label>
                                        {!! Form::date('date', null, ['class' => 'form-control', 'placeholder' => 'Meeting Date']) !!}
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="venue">Meeting Venue:</label>
                                         {!! Form::text('venue', null, ['class' => 'form-control', 'placeholder' => 'Meeting Venue']) !!}
                                </div>
                                <div class="form-group col-md-12">
                                    <label>Participant</label>
                                    {!! Form::select('user_id[]', $user, null, ['class' => 'form-control','id'=>'participant','multiple']) !!}
                                </div>
                                <div class="box-footer">
                                    <button type="button" name="add" id="add" class="btn btn-primary pull-left">Select new participant</button>
                                </div>
                                <br />
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered" id="new_participant_data">
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Phone Number</th>
                                                    <th>Position</th>
                                                    <th>Organization</th>
                                                    <th>Email</th>
                                                    <th>Details</th>
                                                    <th>Remove</th>
                                                </tr>
                                            </table>
                                        </div>
                                    <br />
                                <div id="new_participant_dialog" title="Add new Participant">
                                    <div class="form-group">
                                        <label>Name</label>
                                        <input type="text" name="name" id="name" class="form-control" />
                                        <span id="error_name" class="text-danger"></span>
                                    </div>
                                    <div class="form-group">
                                        <label>Phone Number</label>
                                        <input type="number" name="phone_number" id="phone_number" class="form-control" />
                                        <span id="error_phone_number" class="text-danger"></span>
                                    </div>
                                    <div class="form-group">
                                        <label>Position</label>
                                        <input type="text" name="position" id="position" class="form-control" />
                                        <span id="error_position" class="text-danger"></span>
                                    </div>
                                    <div class="form-group">
                                        <label>Organization</label>
                                        <input type="text" name="organization_name" id="organization_name" class="form-control" />
                                        <span id="error_organization_name" class="text-danger"></span>
                                    </div>
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="text" name="email" id="email" class="form-control" />
                                        <span id="error_email" class="text-danger"></span>
                                    </div>
                                    <div class="form-group">
                                        <input type="hidden" name="row_id" id="hidden_row_id" />
                                        <button type="button" name="save" id="save" class="btn btn-success">Save</button>
                                    </div>
                                </div>
                                <div id="action_alert" title="Action">

                                </div>
                             
                                <div class="form-group col-md-12">
                                    <label for="agenda">Meeting Agenda:</label>
                                        {!! Form::textarea('agenda', null, ['class' => 'form-control', 'rows' => '2','id' =>'agenda']) !!}
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="estimated_meeting_period">Estimated Meeting Period:</label>
                                        {!! Form::time('estimated_meeting_period', null, ['class' => 'form-control', 'placeholder' => 'Meeting Meeting Period']) !!}
                                </div>
                                    <!-- </div> -->
                                <div class="box-footer">
                                    <button type="submit" class="btn btn-primary pull-right">Submit</button>
                                </div>
                            {!! Form::close() !!}
                    </div>
                </div>
            </div>   
        </div>
    </section>
@endsection

@section('scripts')
<script>
$(document).ready(function(){
    $(function () {
        CKEDITOR.replace( 'agenda' );
        $('#participant').select2();
    })
    var count = 0;

    $('#new_participant_dialog').dialog({
        autoOpen:false,
        width:400
    });
    $('#add').click(function(){
        $('#new_participant_dialog').dialog('option','title','Add new Participant');
        $('#name').val('');
        $('#phone_number').val('');
        $('#position').val('');
        $('#organization_name').val('');
        $('#email').val('');
        $('#name').css('border-color','');
        $('#phone_number').css('border-color','');
        $('#position').css('border-color','');
        $('#organization_name').css('border-color','');
        $('#email').css('border-color','');
        $('#save').text('Save');
        $('#new_participant_dialog').dialog('open');
    });

    $('#save').click(function(){
        var error_name = '';
        var error_phone_number = '';
        var error_position = '';
        var error_organization_name = '';
        var error_email = '';
        if($('#name').val() == '')
        {
            error_name = 'Name is required';
            $('#error_name').text(error_name);
            $('#name').css('border-color', '#cc0000');
            name = '';
        }
        else
        {
            error_name = '';
            $('#error_name').text(error_name);
            $('#name').css('border-color','');
            name = $('#name').val();
        }
        if($('#phone_number').val() == '')
        {
            error_phone_number = 'phone number is required';
            $('#error_phone_number').text(error_phone_number);
            $('#phone_number').css('border-color', '#cc0000');
            phone_number = '';
        }
        else
        {
            error_phone_number = '';
            $('#error_phone_number').text(error_phone_number);
            $('#phone_number').css('border-color','');
            phone_number = $('#phone_number').val();
        }
        if($('#position').val() == '')
        {
            error_postion = 'position is required';
            $('#error_position').text(error_position);
            $('#position').css('border-color', '#cc0000');
            position = '';
        }
        else
        {
            error_position = '';
            $('#error_position').text(error_position);
            $('#position').css('border-color','');
            position = $('#position').val();
        }
        if($('#organization_name').val() == '')
        {
            error_organization_name = 'organization name is required';
            $('#error_organization_name').text(error_organization_name);
            $('#organization_name').css('border-color', '#cc0000');
            organization_name = '';
        }
        else
        {
            error_organization_name = '';
            $('#error_organization_name').text(error_organization_name);
            $('#organization_name').css('border-color','');
            organization_name = $('#organization_name').val();
        }
        if($('#email').val() == '')
        {
            error_email = 'email is required';
            $('#error_email').text(error_email);
            $('#email').css('border-color', '#cc0000');
            email = '';
        }
        else
        {
            error_email = '';
            $('#error_email').text(error_email);
            $('#email').css('border-color','');
            email = $('#email').val();
        }
        if(error_name != '' || error_phone_number != '' || error_position != '' || error_organization_name != '' || error_email != '')
        {
            return false;
        }
        else
        {
            if($('#save').text() == 'Save')
            {
                count = count + 1;
                output = '<tr id="row_'+count+'">';
                output += '<td>'+name+' <input type="hidden" name="new['+count+'][name]" id="name'+count+'" class="name" value="'+name+'"/></td>';
                output += '<td>'+phone_number+' <input type="hidden" name="new['+count+'][phone_number]" id="phone_number'+count+'" class="phone_number" value="'+phone_number+'"/></td>';
                output += '<td>'+position+' <input type="hidden" name="new['+count+'][position]" id="position'+count+'" class="position" value="'+position+'"/></td>';
                output += '<td>'+organization_name+' <input type="hidden" name="new['+count+'][organization_name]" id="organization_name'+count+'" class="organization_name" value="'+organization_name+'"/></td>';
                output += '<td>'+email+' <input type="hidden" name="new['+count+'][email]" id="email'+count+'" class="email" value="'+email+'"/></td>';
                output += '<td><button type="button" name="view_details" class="btn btn-warning btn-xs view_details" id="'+count+'"> View</button></td>';
                output += '<td><button type="button" name="remove_details" class="btn btn-danger btn-xs remove_details" id="'+count+'"> Remove</button></td>';
                output +='</tr>';
                $('#new_participant_data').append(output);
            }
            else
            {
                var row_id = $('#hidden_row_id').val();
                output = '<td>'+name+' <input type="hidden" name="new['+row_id+'][name]" id="name'+row_id+'" class="name" value="'+name+'"/></td>';
                output += '<td>'+phone_number+' <input type="hidden" name="new['+row_id+'][phone_number]" id="phone_number'+row_id+'" class="phone_number" value="'+phone_number+'"/></td>';
                output += '<td>'+position+' <input type="hidden" name="new['+row_id+'][position]" id="position'+row_id+'" class="position" value="'+position+'"/></td>';
                output += '<td>'+organization_name+' <input type="hidden" name="new['+row_id+'][organization_name]" id="organization_name'+row_id+'" class="organization_name" value="'+organization_name+'"/></td>';
                output += '<td>'+email+' <input type="hidden" name="new['+row_id+'][email]" id="email'+row_id+'" class="email" value="'+email+'"/></td>';
                output += '<td><button type="button" name="view_details" class="btn btn-warning btn-xs view_details" id="'+row_id+'"> View</button></td>';
                output += '<td><button type="button" name="remove_details" class="btn btn-danger btn-xs remove_details" id="'+row_id+'"> Remove</button></td>';
                $('#row_'+row_id+'').html(output);
            }
            $('#new_participant_dialog').dialog('close');
        }

    });

    $(document).on('click','.view_details',function(){
        var row_id = $(this).attr("id");
        var name = $('#name'+row_id+'').val();
        var phone_number = $('#phone_number'+row_id+'').val();
        var position = $('#position'+row_id+'').val();
        var organization_name = $('#organization_name'+row_id+'').val();
        var email = $('#email'+row_id+'').val();
        $('#name').val(name);
        $('#phone_number').val(phone_number);
        $('#position').val(position);
        $('#organization_name').val(organization_name);
        $('#email').val(email);
        $('#save').text('Edit');
        $('#hidden_row_id').val(row_id);
        $('#new_participant_dialog').dialog('option','title','Edit New Participant');
        $('#new_participant_dialog').dialog('open');
    });

    $(document).on('click', '.remove_details', function(event){
        event.preventDefault();
        let that = $(this);
        var row_id = $(that).attr("id");
        const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
        })
        swalWithBootstrapButtons.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        reverseButtons: true
        }).then((result) => {
            if (result.value) 
            {
                $('#row_'+row_id+'').remove();   
            }
            else
            {
                return false;
            }
        })
    });

    $('#action_alert').dialog({
            autoOpen:false
    });

    // $('#meeting_form').on('submit', function(event){
    //     event.preventDefault();
    //     var count_data = 0;
    //     $('.name').each(function(){
    //         count_data = count_data + 1;
    //     });
    //     {
    //         var form_data = $(this).serialize();
    //         console.log(form_data);
    //         $.ajax({
    //             url:"{{ route('store-meeting') }}",
    //             method:"POST",
    //             data:form_data,
    //             success:function(data)
    //             {
    //                 $('#new_participant_data').find("tr:gt(0)").remove();
    //                 $('#action_alert').html('<p>Data Inserted Successfully</p>');
    //                 $('#action_alert').dialog('open');
    //             }
    //         })
    //     }
    // });
        
});

</script>
@endsection

