<?php

use Illuminate\Database\Seeder;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $department = [
            ["name" => "IT"],
            ["name" => "HR"],
            ["name" => "Management"],
            ["name" => "Accounting"],
        ];

        DB::table('department')->insert($department);
    }
}
