<?php

use Illuminate\Database\Seeder;

class InterestLevelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $interest_level = [
            ["interest_level" => "Not Interested", "order" => 1],
            ["interest_level" => "Satisfactory", "order" => 2],
            ["interest_level" => "Interested", "order" => 3],
            ["interest_level" => "Very Interested", "order" => 4],
        ];

        DB::table('interest_level')->insert($interest_level);
    }
}
